<!DOCTYPE html><html><head>
<title>ns_adp - NaviServer Built-in Commands</title>
<link rel="stylesheet" href="../man.css" type="text/css">
</head>
<!-- Generated from file 'ns_adp.man' by tcllib/doctools with format 'html'
   -->
<!-- ns_adp.n
   -->
<body><div id="man-header">
  <a href="https://wiki.tcl-lang.org/page/NaviServer"><span class="logo">&nbsp;</span><strong>NaviServer</strong></a>
  - programmable web server
</div>
 <hr> [
   <a href="../../toc.html">Main Table Of Contents</a>
&#124; <a href="../toc.html">Table Of Contents</a>
&#124; <a href="../../index.html">Keyword Index</a>
 ] <hr>
<div class="doctools">
<h1 class="doctools_title">ns_adp(n) 4.99.20 naviserver &quot;NaviServer Built-in Commands&quot;</h1>
<div id="name" class="doctools_section"><h2><a name="name">Name</a></h2>
<p>ns_adp - ADP introduction and operation</p>
</div>
<div id="toc" class="doctools_section"><h2><a name="toc">Table Of Contents</a></h2>
<ul class="doctools_toc">
<li class="doctools_section"><a href="#toc">Table Of Contents</a></li>
<li class="doctools_section"><a href="#section1">Description</a></li>
<li class="doctools_section"><a href="#section2">CONFIGURATION</a></li>
<li class="doctools_section"><a href="#see-also">See Also</a></li>
<li class="doctools_section"><a href="#keywords">Keywords</a></li>
</ul>
</div>
<div id="section1" class="doctools_section"><h2><a name="section1">Description</a></h2>
<p>Several commands, normally beginning with the  ns_adp  prefix, are used
to support  NaviServer Dynamic Pages , or ADPs . ADPs are a server-side
environment for embedding Tcl code within static text blocks
(typically HTML or XML). The Tcl code is normally delimited
within  &lt;%  and  %&gt;  or  &lt;%=  and  %&gt;  tags and can be used
to generate additional text or for any other purpose, e.g.,
updating a database.</p>
<p>The  &lt;% ...script... %&gt;  is used for cases where
the result of the Tcl script is ignored while the
&lt;%=...script %&gt;  syntax is used to append the script
result to the output buffer. In either case, the
ns_adp_puts  command can be used to add content to the
output buffer. A simple ADP file could contain:</p>
<pre class="doctools_example">
 &lt;html&gt;
 &lt;head&gt;&lt;title&gt;Hello from &lt;%=[ns_info hostname]%&gt;&lt;/title&gt;&lt;/head&gt;
 &lt;body&gt;
 Time is: &lt;%=[clock format [clock seconds]]%&gt;
 Four links:
 &lt;% for {set i 0} {$i &lt; 4} {incr i} { ns_adp_puts &quot;&lt;a href=/link/$i.htm&gt;Link $i&lt;/a&gt;&lt;br&gt;&quot; } %&gt;
 &lt;/body&gt;
 &lt;/html&gt;
</pre>
<p>Accessing this page would generate output similar to:</p>
<pre class="doctools_example">
 &lt;html&gt;
 &lt;head&gt;&lt;title&gt;Hello from jgdavidson.local&lt;/title&gt;&lt;/head&gt;
 &lt;body&gt;
 Time is: Mon Aug 01 22:15:18 EDT 2005
 Four links: &lt;a href=/link/0.htm&gt;Link 0&lt;/a&gt;&lt;br&gt;
 &lt;a href=/link/1.htm&gt;Link 1&lt;/a&gt;&lt;br&gt;
 &lt;a href=/link/2.htm&gt;Link 2&lt;/a&gt;&lt;br&gt;
 &lt;a href=/link/3.htm&gt;Link 3&lt;/a&gt;&lt;br&gt;
 &lt;/body&gt;
 &lt;/html&gt;
</pre>
<p>ADP processing normally occurs in the context of an HTTP transaction when
a URL request is mapped to an ADP file in the
server's page root. (see  ADP CONFIGURATION
below for details on configuring this mapping). The ADP
request processing code allocates a Tcl interpreter and
includes the corresponding ADP file. Output generated during
execution of the ADP is sent as a normal HTTP response,
using default status code of &quot;200 OK&quot; and the mime
type which corresponds to the ADP file extensions, normally
.adp and text/html (commands such as  ns_adp_mimetype
can be used to control the eventual response type).</p>
<p>An ADP can include additional ADP files with the  ns_adp_include
command or evaluate ADP text/script code directly with
<b class="cmd"><a href="ns_adp_parse.html">ns_adp_parse</a></b>. This capability enables are large degree
of reuse of presentation and code between applications. Each
such included file or ADP string evaluation is performed in
its own call frame  similar to a Tcl procedure
with a local variable namespace. Arguments can be passed to
new call frames and then accessed with commands such as
 ns_adp_argv . When necessary, commands such as
 ns_adp_abort  provide limited support to interrupt
and/or return from within an ADP, unwinding the ADP call
stack to the underlying C-level request processing code.</p>
<p>NaviServer can be configured to execute ADP's placed with other static
files within a virtual server's pages directory via
the  map  parameter in the ADP server config
section, for example:</p>
<pre class="doctools_example">
 ns_section ns/server/server1/adp
 ns_param map /*.adp ns_param map {/stories/*.adp 60}
</pre>
<p>The first map will evaluate all files which end in  .adp  and do not have
more specific mappings (such as the second map). The second
config map will execute files which end with  .adp
located under the  /stories  directly and also
specifies a cache timeout in seconds. In this case, results
will be retained and returned to subsequent requests without
re-executing the ADP for up to 60 seconds (see the
 -cache  parameter to the  ns_adp_include  command
for more details).</p>
<p>Alternatively, arbitrary URL's may be mapped to individual ADP files
using the  ns_register_adp  command. This command would
normally be included in a virtual-server initialization
scripts within the  modules/tcl/  server
subdirectory.</p>
<p>By default, errors within an ADP script block are reported in the server log
and interrupt execution of the current block only;
subsequent text and script blocks continue to be processed
and no error message is included in the output. This
approach is highly defensive and has the benefit of
generating a valid, if partial, responses after minor
errors. A negative aspect of this approach is that, without
careful monitoring of the server log, such errors can easily
be ignored.</p>
<p>The default error handling behavior can be modified by settings one or more
virtual-server configuration flags:</p>
<pre class="doctools_example">
 ns_section ns/server/server1/adp
 ns_param stricterror false; # Interrupt execution on any error.
 ns_param displayerror false; # Include error message in output.
 ns_param detailerror true; # Include connection details messages.
</pre>
<p>These flags, along with other options, can be queried or modified for an
individual ADP execution stream via the  ns_adp_ctl.</p>
<p>By default, each Tcl block is independent of other blocks and must be a
complete script. As a consequence, no conditional
code can spanother ADP blocks. E.g., the following does not work:</p>
<pre class="doctools_example">
 &lt;% foreach elem $list { %&gt; Here is an &lt;%=$elem%&gt; element. &lt;% } %&gt;
</pre>
<p>This behavior can be changed with the  singlescript  config option or via
the  ns_adp_ctl  command which instructs the ADP parser
to converts all text/code blocks within an ADP into a single
Tcl script block:</p>
<pre class="doctools_example">
 ns_section ns/server/server1/adp
 ns_param singlescript false; # Combine code blocks into one scripts.
</pre>
<p>Setting this option would convert the script above into the following
equivalent:</p>
<pre class="doctools_example">
 &lt;%
  foreach elem $list {
    ns_adp_puts -nonewline &quot;\n Here is an&quot;
    ns_adp_puts -nonewline $elem
    ns_adp_puts -nonewline &quot; element.\n&quot;
  }
 %&gt;
</pre>
<p>Note that this option combines scripts within a particular ADP file, it
does not combine scripts which span multiple included
ADP's. In addition, error semantics described above
apply to the combined script and any error within any block
combined into a single script will stop execution of the
entire included page.</p>
<p>Output including accumulated text blocks and output generated by Tcl script
blocks is normally buffered internally until the end of the
connection. Once complete, a single response is generated
which follows HTTP response headers indicating the resulting
content length. The content may optionally be gzip
compressed first.</p>
<p>Alternatively, an incremental response can be generated either
in response to calling the  ns_adp_stream  or  ns_adp_flush
commands or automatically due to buffer overflow. In this
case, an HTTP response will be generated on the first flush
which specifies incremental content using HTTP/1.1
chunked-encoding. Forcing a connection into streaming mode
can be useful for certain long running requests where
it is reasonable to expect the browser can render
incremental respnoses.</p>
<p>The size of the internal buffer and gzip compression options can
be set with corresponding server and ADP config options. Note both the
virtual-server wide gzip and ADP gzip options must be
enabled to support compression of ADP output.</p>
<pre class="doctools_example">
 ns_section ns/server/server1/adp
 ns_param gzip true     ;# Enable ADP output compression.
 ns_param bufsize 1MB   ;# Buffer size, 1MB default.
</pre>
<p>The ADP interface uses the server's mimetype configuration
to map file extensions to charsets and corresponding encoding. This
configuration is necessary to ensure the file text and
script blocks are properly converted to UTF-8 for use
internally. This mimetype is also used to set the character
output encoding although the  ns_conn encoding  option
can be used to override the encoding if necessary.</p>
</div>
<div id="section2" class="doctools_section"><h2><a name="section2">CONFIGURATION</a></h2>
<p>ADP pages can be enabled per-virtual-server in the configuration file.</p>
<pre class="doctools_example">
 <b class="cmd">ns_section</b> &quot;ns/server/server1/adp&quot;
 <b class="cmd">ns_param</b>   map     /*.adp
 ...
</pre>
<p>A <i class="term">map</i> entry is used to register the ADP page handler for each of the GET,
HEAD and POST methods on the given URL. There may be zero or more map entries.</p>
<p>The following parameters provide a hook for running common code for each ADP
either at the beginning of processing or for handling errors.</p>
<dl class="doctools_definitions">
<dt>startpage</dt>
<dd><p>The path of an ADP page which is run before the requested ADP page.
Default: none.</p></dd>
<dt>errorpage</dt>
<dd><p>The path of an ADP page which is run when a code within an ADP raises an error
which is not caught. Default: none.</p></dd>
</dl>
<p>The following parameters control memory usage.</p>
<dl class="doctools_definitions">
<dt>cachesize</dt>
<dd><p>The size in bytes of the per-virtual-server page cache. This is the cache of ADP
pages as read from disk and converted to an efficient form for evaluation. In
addition, a separate cache of script blocks is kept per-thread, which is not
controlled by this parameter.
The value can be specified in memory units (kB, MB, GB, KiB, MiB, GiB).
Default: 5MB.</p></dd>
<dt>bufsize</dt>
<dd><p>The size in bytes of the ADP output buffer. The buffer is flushed to the client
when full, or each time a new chunk is appended when streaming is
enabled.
The value can be specified in memory units (kB, MB, GB, KiB, MiB, GiB).
Default: 1MB.</p></dd>
<dt>tracesize</dt>
<dd><p>The number of bytes of each text and script block which will be dumped to the
error log when the <i class="term"><a href="../../index.html#trace">trace</a></i> option is enabled. Default: 40.</p></dd>
</dl>
<p>The following parameters set the default options for the ADP engine. They can be
customised per-URL using the <b class="option">-options</b> flag of the <b class="cmd">ns_register_adp</b>
command, or at run-time for each page using the <b class="cmd"><a href="ns_adp_ctl.html">ns_adp_ctl</a></b> command.</p>
<p>See <b class="cmd"><a href="ns_adp_ctl.html">ns_adp_ctl</a></b> for details on each option.</p>
<dl class="doctools_definitions">
<dt>cache</dt>
<dd><p>Default: off.</p></dd>
<dt>stream</dt>
<dd><p>Default: off.</p></dd>
<dt>enablexpire</dt>
<dd><p>Default: off.</p></dd>
<dt>enabledebug</dt>
<dd><p>Default: off.</p></dd>
<dt>safeeval</dt>
<dd><p>Default: off.</p></dd>
<dt>singlescript</dt>
<dd><p>Default: off.</p></dd>
<dt>trace</dt>
<dd><p>Log each text and script block of each ADP page as it is executed. The first n
bytes will be logged, as determined by the <i class="term">tracesize</i> parameter.
Default: off.</p></dd>
<dt>detailerror</dt>
<dd><p>Default: on.</p></dd>
<dt>stricterror</dt>
<dd><p>Default: off.</p></dd>
<dt>displayerror</dt>
<dd><p>Default: off.</p></dd>
<dt>trimspace</dt>
<dd><p>Default: off.</p></dd>
<dt>autoabort</dt>
<dd><p>Default: enabled.</p></dd>
</dl>
<p>The following parameter...</p>
<dl class="doctools_definitions">
<dt>debuginit</dt>
<dd><p>The command which is called when ADP page debugging is initiated. The parameter
<i class="term">enabledebug</i> must be on for this to take effect.
Default: <b class="cmd">ns_adp_debuginit</b>.</p></dd>
</dl>
</div>
<div id="see-also" class="doctools_section"><h2><a name="see-also">See Also</a></h2>
<p>ns_adp, <a href="ns_adp_abort.html">ns_adp_abort</a>, <a href="ns_adp_ctl.html">ns_adp_ctl</a>, <a href="ns_adp_include.html">ns_adp_include</a>, <a href="ns_adp_parse.html">ns_adp_parse</a>, <a href="ns_adp_puts.html">ns_adp_puts</a>, <a href="ns_adp_register.html">ns_adp_register</a></p>
</div>
<div id="keywords" class="doctools_section"><h2><a name="keywords">Keywords</a></h2>
<p><a href="../../index.html#adp">ADP</a>, <a href="../../index.html#configuration">configuration</a>, <a href="../../index.html#gzip">gzip</a>, <a href="../../index.html#ns_conn">ns_conn</a></p>
</div>
<div id="man-footer">
  
</div>
</div></body></html>
