<!DOCTYPE html><html><head>
<title>ns_proxy - NaviServer Module Commands</title>
<link rel="stylesheet" href="../man.css" type="text/css">
</head>
<!-- Generated from file 'ns_proxy.man' by tcllib/doctools with format 'html'
   -->
<!-- ns_proxy.n
   -->
<body><div id="man-header">
  <a href="https://wiki.tcl-lang.org/page/NaviServer"><span class="logo">&nbsp;</span><strong>NaviServer</strong></a>
  - programmable web server
</div>
 <hr> [
   <a href="../../toc.html">Main Table Of Contents</a>
&#124; <a href="../toc.html">Table Of Contents</a>
&#124; <a href="../../index.html">Keyword Index</a>
 ] <hr>
<div class="doctools">
<h1 class="doctools_title">ns_proxy(n) 4.99.20 nsproxy &quot;NaviServer Module Commands&quot;</h1>
<div id="name" class="doctools_section"><h2><a name="name">Name</a></h2>
<p>ns_proxy - Execute Tcl scripts in an external process</p>
</div>
<div id="toc" class="doctools_section"><h2><a name="toc">Table Of Contents</a></h2>
<ul class="doctools_toc">
<li class="doctools_section"><a href="#toc">Table Of Contents</a></li>
<li class="doctools_section"><a href="#synopsis">Synopsis</a></li>
<li class="doctools_section"><a href="#section1">Description</a></li>
<li class="doctools_section"><a href="#section2">COMMANDS</a></li>
<li class="doctools_section"><a href="#section3">ERROR HANDLING</a></li>
<li class="doctools_section"><a href="#section4">CONFIGURATION</a></li>
<li class="doctools_section"><a href="#section5">EXAMPLES</a></li>
<li class="doctools_section"><a href="#see-also">See Also</a></li>
<li class="doctools_section"><a href="#keywords">Keywords</a></li>
</ul>
</div>
<div id="synopsis" class="doctools_section"><h2><a name="synopsis">Synopsis</a></h2>
<div class="doctools_synopsis">
<ul class="doctools_syntax">
<li><a href="#1"><b class="cmd">ns_proxy active</b> <i class="arg">pool</i> <span class="opt">?<i class="arg">handle</i>?</span></a></li>
<li><a href="#2"><b class="cmd">ns_proxy cleanup</b></a></li>
<li><a href="#3"><b class="cmd">ns_proxy clear</b> <i class="arg">pool</i> <span class="opt">?<i class="arg">handle</i>?</span></a></li>
<li><a href="#4"><b class="cmd">ns_proxy configure</b> <i class="arg">pool</i> <span class="opt">?-key val -key val...?</span></a></li>
<li><a href="#5"><b class="cmd">ns_proxy eval</b> <i class="arg">handle</i> <i class="arg">script</i> <span class="opt">?<i class="arg">timeout</i>?</span></a></li>
<li><a href="#6"><b class="cmd">ns_proxy free</b> <i class="arg">pool</i></a></li>
<li><a href="#7"><b class="cmd">ns_proxy get</b> <i class="arg">pool</i> <span class="opt">?<b class="option">-handles <i class="arg">n</i></b>?</span> <span class="opt">?<b class="option">-timeout <i class="arg">timeout</i></b>?</span></a></li>
<li><a href="#8"><b class="cmd">ns_proxy handles</b></a></li>
<li><a href="#9"><b class="cmd">ns_proxy ping</b> <i class="arg">handle</i></a></li>
<li><a href="#10"><b class="cmd">ns_proxy pools</b></a></li>
<li><a href="#11"><b class="cmd">ns_proxy put</b> <i class="arg">handle</i></a></li>
<li><a href="#12"><b class="cmd">ns_proxy recv</b> <i class="arg">handle</i></a></li>
<li><a href="#13"><b class="cmd">ns_proxy release</b> <i class="arg">handle</i></a></li>
<li><a href="#14"><b class="cmd">ns_proxy send</b> <i class="arg">handle</i> <i class="arg">script</i></a></li>
<li><a href="#15"><b class="cmd">ns_proxy stats</b> <i class="arg">pool</i></a></li>
<li><a href="#16"><b class="cmd">ns_proxy stop</b> <i class="arg">pool</i> <span class="opt">?<i class="arg">handle</i>?</span></a></li>
<li><a href="#17"><b class="cmd">ns_proxy wait</b> <i class="arg">handle</i> <span class="opt">?<i class="arg">timeout</i>?</span></a></li>
</ul>
</div>
</div>
<div id="section1" class="doctools_section"><h2><a name="section1">Description</a></h2>
<p><b class="cmd">ns_proxy</b> provides a simple, robust proxy mechanism to evaluate
Tcl scripts in a separate, pipe-connected process. This approach
can be useful both to isolate potentially thread-unsafe code outside
the address space of a multithreaded process such as NaviServer or
to enable separation and timeout of potentially misbehaving, long
running scripts.</p>
<p>The command is provided by the <em>nsproxy</em> dynamic library which
can be loaded into an interpreter via the Tcl <em>load</em> command,
for example:</p>
<pre class="doctools_example">
 load &quot;<b class="file">/usr/local/lib/libnsproxy.so</b>&quot;
 <b class="cmd">ns_proxy</b> ...
</pre>
<p>It is also possible to load the library into all interpreters of
a NaviServer virtual server by specifying an <em>nsproxy.so</em> entry
in the server's module config entry, for example:</p>
<pre class="doctools_example">
 <b class="cmd">ns_section</b> &quot;ns/server/server1/modules&quot;
 <b class="cmd">ns_param</b> nsproxy  nsproxy.so
</pre>
<p>When loaded, the library adds the single <b class="cmd">ns_proxy</b> command with
takes multiple options as described below. Proxies (i.e. worker
processes) are normally created on demand when requested and connected
to the parent process via pipes used to send scripts and receive response.
Proxies remain active until the parent process exits, effectively closing
all pipes to the worker processes, or when their idle timer expires, depending
on the setup of the <i class="arg">pool</i> (see <b class="cmd">ns_proxy configure</b>).</p>
<p>Proxies are obtained from their corresponding <i class="arg">pool</i> by means of the
<b class="cmd">ns_proxy get</b> command. Only the thread that obtained the proxy can use
it to communicate with the worker process. In order to allow other threads to
use the same proxy, the thread must return (via the <b class="cmd">ns_proxy put</b> or
<b class="cmd">ns_proxy cleanup</b> commands) the proxy back to its corresponding
pool. One thread can obtain one or a bunch of proxies from a pool in one step.
It cannot, however, repeatedly obtain proxy by proxy in a loop, as this may
lead to difficult-to-trace deadlock situation (see <b class="cmd">ns_proxy get</b> command).</p>
<p>All the <i class="arg">timeout</i> values below (no matter if these are used
in positional or arguments, or as parameters in the configuration file) can
be specified with time units. If the <i class="arg">timeout</i> value time has no time
unit, seconds are assumed.</p>
</div>
<div id="section2" class="doctools_section"><h2><a name="section2">COMMANDS</a></h2>
<dl class="doctools_definitions">
<dt><a name="1"><b class="cmd">ns_proxy active</b> <i class="arg">pool</i> <span class="opt">?<i class="arg">handle</i>?</span></a></dt>
<dd><p>Returns a list of currently evaluating scripts in proxies for
the given <i class="arg">pool</i>.</p>
<p>The output is one or more lists, depending on the optional
<span class="opt">?handle?</span> argument. If the optional argument is
given, only the status of the proxy for the given handle is
returned and the result is a one-element list. Otherwise, statuses of
all active proxies for the given <i class="arg">pool</i> are returned and the
result is a list of two or more elements.</p>
<p>Each element itself is a list which includes several keys: <em>handle</em>,
<em>worker</em>, <em>start</em>, <em>script</em> and their associated values.
This format is suitable for filling in a Tcl array with the
<b class="cmd">array set</b> Tcl command.
The <em>handle</em> key contains the handle of the proxy.
The <em>worker</em> key contains the process-id of the worker process.
The <em>start</em> key contains the timestamp with the absolute time
when this proxy has been activated. The timestamp is in format that
<b class="cmd"><a href="../../naviserver/files/ns_time.html">ns_time</a></b> command understands.</p>
<p>The <em>script</em> contains the script passed to the proxy for execution.
It is also possible to view the currently evaluating scripts with the
Unix <em>ps</em> command as the proxy worker process re-writes its command
argument space with the request script before evaluation and clears it
after sending the result.</p></dd>
<dt><a name="2"><b class="cmd">ns_proxy cleanup</b></a></dt>
<dd><p>Releases any handles from any pools currently owned by a thread.</p>
<p>This command is intended to be used as part of a garbage collection
step. Calling this command within NaviServer is not necessary as the
module registers a trace to release all handles via the
<b class="cmd">ns_ictl trace deallocate</b> facility when interpreters are deallocated
after some transaction, for example, at the end of a connection.</p></dd>
<dt><a name="3"><b class="cmd">ns_proxy clear</b> <i class="arg">pool</i> <span class="opt">?<i class="arg">handle</i>?</span></a></dt>
<dd><p>Stop all worker processes attached to free proxies for the given <i class="arg">pool</i>.
If the optional <i class="arg">handle</i> is given, it stops the process only for
that handle.</p></dd>
<dt><a name="4"><b class="cmd">ns_proxy configure</b> <i class="arg">pool</i> <span class="opt">?-key val -key val...?</span></a></dt>
<dd><p>Configures options for the <i class="arg">pool</i>. The <i class="arg">pool</i> is created with
default options if it does not already exist. Default options for the
<i class="arg">pool</i> are taken from the NaviServer configuration file under the
section &quot;ns/server/$server/module/nsproxy&quot;. In case
the library is loaded in plain Tcl shell, default configuration options
are fixed and cannot be changed w/o recompiling the code.
Configurable options include:</p>
<dl class="doctools_options">
<dt><b class="option">-env</b> <i class="arg">setname</i></dt>
<dd><p>Initializes the worker's process environment with keys/values
passed in the named NaviServer set (see command <b class="cmd"><a href="../../naviserver/files/ns_set.html">ns_set</a></b>).</p></dd>
<dt><b class="option">-evaltimeout</b> <i class="arg">timeout</i></dt>
<dd><p>Specifies the maximum time to wait for a script to be evaluated in
a proxy. This parameter can be overridden on a per-call basis with
the optional <span class="opt">?timeout?</span> parameter to <b class="cmd">ns_proxy eval</b>.
The default is 0 milliseconds i.e. infinite.</p></dd>
<dt><b class="option">-exec</b> <i class="arg">program</i></dt>
<dd><p>Specifies the filename of a worker proxy program.  The default is
<em>nsproxy</em> in the <em>bin</em> subdirectory of the NaviServer
run time. It is possible to create a custom program and enter the
proxy event loop with the <em>Ns_ProxyMain</em> application startup
routine; see the source code for details.</p></dd>
<dt><b class="option">-gettimeout</b> <i class="arg">timeout</i></dt>
<dd><p>Specifies the maximum time to wait to allocate handles from the pool.
The default is 5 seconds.</p></dd>
<dt><b class="option">-idletimeout</b> <i class="arg">timeout</i></dt>
<dd><p>Specifies the maximum time for an idle worker process to live.
Minimum value is 5 seconds. After expiry of the idle
timeout, the reaper thread will close the connection pipe and
wait <b class="option">-waittimeout</b> for the process to die.
If the timeout is exceeded, the reaper will send a SIGTERM
signal and finally a SIGKILL signal (waiting <b class="option">-waittimeout</b>
in between) to ensure the process eventually exits.</p>
<p>Worker processes whose handles are already attached to some Tcl
interps by the means of the <b class="cmd">ns_proxy get</b> command) are not
expired automatically. The idle timer starts to count at the moment
their handles are put back to the pool by the <b class="cmd">ns_proxy put</b>
or <b class="cmd">ns_proxy cleanup</b> command.</p></dd>
<dt><b class="option">-init</b> <i class="arg">script</i></dt>
<dd><p>Specifies a script to evaluate when proxies are started.  This can
be used to load additional libraries and/or source script files.
The default is no script.</p></dd>
<dt><b class="option">-logminduration</b> <i class="arg">timeout</i></dt>
<dd><p>Specifies a time limit for logging (long) eval operations to the system
log (similar to &quot;logminduration&quot; in the db drivers). Set it to a high
value to avoid logging (e.g. 1d). The default is 1s.</p></dd>
<dt><b class="option">-maxruns</b> <i class="arg">n</i></dt>
<dd><p>Sets the maximum number of activation of the proxy worker process.
When the limit it reached, the worker process is automatically restarted.</p></dd>
<dt><b class="option">-maxworkers</b> <i class="arg">n</i></dt>
<dd><p>Sets the maximum number of proxy worker processes. Requests for
proxies beyond the maximum will result in requesting threads
waiting for existing proxies to be available instead of creating
new proxy processes. Setting this value to 0 disables the pool,
causing all subsequent allocation requests to fail immediately
(currently allocated proxies, if any, remain valid).</p></dd>
<dt><b class="option">-reinit</b> <i class="arg">script</i></dt>
<dd><p>Specifies a script to evaluate after being allocated and before
being returned to the caller. This can be used to re-initialize
the worker state. The default is no script.</p></dd>
<dt><b class="option">-sendtimeout</b> <i class="arg">timeout</i></dt>
<dd></dd>
<dt><b class="option">-recvtimeout</b> <i class="arg">timeout</i></dt>
<dd><p>Specifies the maximum time to wait to send a script and receive a
result from a proxy.  The default is 1 second which assumes
minimal delay sending and receiving reasonably sized scripts and 
results over the connecting pipe.</p></dd>
<dt><b class="option">-waittimeout</b> <i class="arg">timeout</i></dt>
<dd><p>Specifies the maximum time to wait for a proxy to exit. The wait
is performed in a dedicated reaper thread. The reaper will close
the connection pipe and wait the given timeout. If the timeout is
exceeded, the reaper will send a SIGTERM signal and finally a SIGKILL
signal to ensure the process eventually exits. The default is 1
second which should be ample time for a graceful exit unless
the process is hung executing a very long, misbehaving script,
resulting in a more disruptive SIGTERM or SIGKILL.</p></dd>
</dl></dd>
<dt><a name="5"><b class="cmd">ns_proxy eval</b> <i class="arg">handle</i> <i class="arg">script</i> <span class="opt">?<i class="arg">timeout</i>?</span></a></dt>
<dd><p>Evaluates <i class="arg">script</i> in the proxy specified by <i class="arg">handle</i>.  The
optional <span class="opt">?timeout?</span> argument specifies a maximum time to wait for
the command to complete before raising an error (see ERROR HANDLING
below for details on handling errors).</p>
<p>Alternatively, the <i class="arg">handle</i> itself may be used as Tcl command like
in the example below:</p>
<pre class="doctools_example">
 set handle [<b class="cmd">ns_proxy</b> get mypool]
 $handle &quot;short_running_proc&quot;
 $handle &quot;long_running_proc&quot; 20000
</pre>
</dd>
<dt><a name="6"><b class="cmd">ns_proxy free</b> <i class="arg">pool</i></a></dt>
<dd><p>Returns a list of all free proxies for the given <i class="arg">pool</i>. Free
proxies are those which are left in the pool queue waiting to
be used by the <b class="cmd">ns_proxy get</b> command. Some proxies may have
an active worker process attached, some not. If a worker process is
not attached to the free proxy, a new one will be created as soon
as the proxy is requested by some thread.</p></dd>
<dt><a name="7"><b class="cmd">ns_proxy get</b> <i class="arg">pool</i> <span class="opt">?<b class="option">-handles <i class="arg">n</i></b>?</span> <span class="opt">?<b class="option">-timeout <i class="arg">timeout</i></b>?</span></a></dt>
<dd><p>Returns one or more handles to proxies from the specified <i class="arg">pool</i>.</p>
<p>The <i class="arg">pool</i> will be created with default options if it does not
already exist. The optional <b class="option">-handle</b> can be used to
specify the number of handles to allocate, the default being 1.</p>
<p>The optional <span class="opt">?-timeout timeout?</span> arguments specifies the maximum
time to wait for the handles to become
available before raising an error (see ERROR HANDLING below
for details on handling errors).</p>
<p>Requesting more than one handle in a single call (if more than one
handle is required) is necessary as it is an error to request
handles from a <i class="arg">pool</i> from which handles are already owned by
the thread. This restriction is implemented to avoid possible
deadlock conditions.</p>
<p>The handle returned by this command can be used as a scalar value for
other <b class="cmd">ns_proxy</b> commands, or it can be used as Tcl command itself
(see <b class="cmd">ns_proxy eval</b> for more information).</p>
<p>The proxy <i class="arg">pool</i> naming convention allows proxy worker to be started
under different Unix UID/GID then the server itself. For that to work,
the server must be running under root user (UID = 0). The naming
convention is simple: pool_name:&lt;optional_user_id&gt;:&lt;optional_group_id&gt;.</p>
<p>For example, to start the proxy for the pool &quot;mypool&quot; with user UID
of 100 the pool name can be constructed as: &quot;mypool:100&quot;. To start the
proxy with UID of 100 and group GID of 200: &quot;mypool:100:200&quot;. Instead
of numeric values user/group names can also be used.</p>
<p>Beware: if the main server is not running under privileged root user,
the startup of the proxy under some alternative UID/GID may/will fail.</p></dd>
<dt><a name="8"><b class="cmd">ns_proxy handles</b></a></dt>
<dd><p>Returns list of all proxies allocated for the current interpreter.</p></dd>
<dt><a name="9"><b class="cmd">ns_proxy ping</b> <i class="arg">handle</i></a></dt>
<dd><p>This command sends a null request to the proxy specified by the
<i class="arg">handle</i> argument. The proxy will be verified alive and restarted
if necessary.  This command is not normally required as the
<b class="cmd">ns_proxy eval</b> command will also verify and restart proxies
as needed.</p></dd>
<dt><a name="10"><b class="cmd">ns_proxy pools</b></a></dt>
<dd><p>Returns a list of all currently defined proxy pools.</p></dd>
<dt><a name="11"><b class="cmd">ns_proxy put</b> <i class="arg">handle</i></a></dt>
<dd><p>This command is alternate name for <b class="cmd">ns_proxy release</b>.</p></dd>
<dt><a name="12"><b class="cmd">ns_proxy recv</b> <i class="arg">handle</i></a></dt>
<dd><p>Reads result from the script from the proxy specified by <i class="arg">handle</i>
(see ERROR HANDLING below for details on handling errors).</p></dd>
<dt><a name="13"><b class="cmd">ns_proxy release</b> <i class="arg">handle</i></a></dt>
<dd><p>Return the proxy <i class="arg">handle</i> to the pool. All handles owned by a
thread to the corresponding pool must be returned before any handles
can be allocated again. Within the server, a call to this routine is
recommended for clarity but not strictly necessary. NaviServer
installs a trace to release all handles at the end of every connection
during interpreter deallocation.</p></dd>
<dt><a name="14"><b class="cmd">ns_proxy send</b> <i class="arg">handle</i> <i class="arg">script</i></a></dt>
<dd><p>Sends <i class="arg">script</i> to the proxy specified by <i class="arg">handle</i>.
(see ERROR HANDLING below for details on handling errors).</p></dd>
<dt><a name="15"><b class="cmd">ns_proxy stats</b> <i class="arg">pool</i></a></dt>
<dd><p>Provide usage statistics in form of a dict from the specified pool.
The dict contains the following keys:
<i class="term">proxies</i>,
<i class="term">waiting</i>,
<i class="term">maxworkers</i>,
<i class="term">free</i>,
<i class="term">used</i>,
<i class="term">requests</i>,
<i class="term">processes</i>,
<i class="term">runtime</i>.</p></dd>
<dt><a name="16"><b class="cmd">ns_proxy stop</b> <i class="arg">pool</i> <span class="opt">?<i class="arg">handle</i>?</span></a></dt>
<dd><p>Stop all worker processes attached to running proxies for the given <i class="arg">pool</i>.
If the optional <i class="arg">handle</i> is given, it stops the process only for
that handle.</p></dd>
<dt><a name="17"><b class="cmd">ns_proxy wait</b> <i class="arg">handle</i> <span class="opt">?<i class="arg">timeout</i>?</span></a></dt>
<dd><p>Waits for results from the proxy specified by <i class="arg">handle</i>.
The optional <i class="arg">timeout</i> argument specifies a maximum time
to wait for the command to complete before raising an
error (see ERROR HANDLING below for details on handling errors).</p></dd>
</dl>
<p>All time units can be specified with and without a time unit
suffix. Valid time units are &quot;ms&quot;, &quot;s&quot;, &quot;m&quot;, &quot;h&quot;, &quot;d&quot;.
If no time unit suffix is specified, seconds are assumed.</p>
</div>
<div id="section3" class="doctools_section"><h2><a name="section3">ERROR HANDLING</a></h2>
<p>Errors generated by a script evaluated in a proxy interpreter are
completely returned to the calling interpreter, including mapping
the <em>errorInfo</em> and <em>errorCode</em> global variables from the
proxy to the parent and raising a Tcl exception. This approach makes
ns_proxy evaluations look very similar to the Tcl <b class="cmd">eval</b>
command.</p>
<p>Errors raised by a failure to communicate with the proxy process
due to a timeout or unexpected process exit are also communicated
back to the parent interpreter as Tcl exceptions.  To distinguish
between these cases, communication related errors set the
<em>errorCode</em> global variable with the first element
<em>NSPROXY</em>.  The second element is one of the following:</p>
<dl class="doctools_definitions">
<dt>EDeadlock</dt>
<dd><p>The interpreter attempted to allocate handles from a pool from which
it already owns one or more handles.</p></dd>
<dt>EExec</dt>
<dd><p>The worker program specified by the <b class="option">-exec program</b> option could
not be started.</p></dd>
<dt>EImport</dt>
<dd><p>The response from the proxy was invalid.</p></dd>
<dt>ERecv</dt>
<dd><p>There was an error receiving the result from the worker process.</p></dd>
<dt>ESend</dt>
<dd><p>There was an error sending the script to the worker process.</p></dd>
<dt>EGetTimeout</dt>
<dd><p>Timeout while waiting to get a proxy handle from the pool.</p></dd>
<dt>EEvalTimeout</dt>
<dd><p>Timeout while waiting for the response from the proxy process after
sending the command for evaluation.</p></dd>
<dt>ERange</dt>
<dd><p>Requested too many proxy handles from the pool</p></dd>
<dt>EIdle</dt>
<dd><p>Proxy is currently in the idle state.</p></dd>
<dt>EInit</dt>
<dd><p>Evaluation of the init script failed.</p></dd>
<dt>EDead</dt>
<dd><p>Proxy handle is currently not connected to any process.</p></dd>
<dt>EBusy</dt>
<dd><p>Proxy handle is currently busy with the evaluation.</p></dd>
</dl>
</div>
<div id="section4" class="doctools_section"><h2><a name="section4">CONFIGURATION</a></h2>
<p>The default settings of the configuration parameters of <b class="cmd">ns_proxy</b>
can be provided in the configuration file of NaviServer.</p>
<pre class="doctools_example">
 # Loading the nxproxy module in the <i class="term">modules</i> section
 # of the server.
 ns_section	ns/server/${server}/modules {
   # ...
   ns_param	nsproxy			${homedir}/bin/nsproxy
   # ...
 }
 
 # Configuring the nsproxy module
 <b class="cmd">ns_section</b> ns/server/${server}/module/nsproxy {
 
   # Proxy program to start
   ns_param	exec			${homedir}/bin/nsproxy
 
   # Timeout when evaluating scripts
   ns_param	evaltimeout		0s
 
   # Timeout when getting proxy handles
   ns_param	gettimeout		0s
 
   # Timeout to send data
   ns_param	sendtimeout		5s
 
   # Timeout to receive results
   ns_param	recvtimeout		5s
 
   # Timeout to wait for workers to die
   ns_param	waittimeout		1s
 
   # Timeout for a worker to live idle
   ns_param	idletimeout		5m
 
   # log eval operations longer than this to the system log
   ns_param	logminduration		1s
 
   # Max number of allowed workers alive
   ns_param	maxworkers		8
 }
</pre>
</div>
<div id="section5" class="doctools_section"><h2><a name="section5">EXAMPLES</a></h2>
<p>The following demonstrates sending a script to a remote proxy:</p>
<pre class="doctools_example">
 set handle [<b class="cmd">ns_proxy</b> get myproxy]
 <b class="cmd">ns_proxy</b> eval $handle {info patchlevel}
 <b class="cmd">ns_proxy</b> release $handle
</pre>
<p>Alternatively, instead of using the scalar handle you can use
the handle directly as a Tcl command:</p>
<pre class="doctools_example">
 set handle [<b class="cmd">ns_proxy</b> get myproxy]
 $handle {info patchlevel}
 rename $handle &quot;&quot;
</pre>
<p>The following demonstrates using multiple proxies:</p>
<pre class="doctools_example">
 <b class="cmd">ns_proxy</b> configure myproxy -maxworkers 10
 set handles [<b class="cmd">ns_proxy</b> get myproxy -handle 10]
 foreach h $handles {
   $h {puts &quot;alive: [pid]&quot;}
 }
 <b class="cmd">ns_proxy</b> cleanup
</pre>
</div>
<div id="see-also" class="doctools_section"><h2><a name="see-also">See Also</a></h2>
<p><a href="../../naviserver/files/ns_job.html">ns_job</a></p>
</div>
<div id="keywords" class="doctools_section"><h2><a name="keywords">Keywords</a></h2>
<p><a href="../../index.html#exec">exec</a>, <a href="../../index.html#module">module</a>, <a href="../../index.html#nsproxy">nsproxy</a>, <a href="../../index.html#pools">pools</a>, <a href="../../index.html#proxy">proxy</a>, <a href="../../index.html#server_built_in">server built-in</a></p>
</div>
<div id="man-footer">
  
</div>
</div></body></html>
