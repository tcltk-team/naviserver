<!DOCTYPE html><html><head>
<title>tcl-libraries - NaviServer Manual</title>
<link rel="stylesheet" href="../man.css" type="text/css">
</head>
<!-- Generated from file 'tcl-libraries.man' by tcllib/doctools with format 'html'
   -->
<!-- tcl-libraries.n
   -->
<body><div id="man-header">
  <a href="https://wiki.tcl-lang.org/page/NaviServer"><span class="logo">&nbsp;</span><strong>NaviServer</strong></a>
  - programmable web server
</div>
 <hr> [
   <a href="../../toc.html">Main Table Of Contents</a>
&#124; <a href="../toc.html">Table Of Contents</a>
&#124; <a href="../../index.html">Keyword Index</a>
 ] <hr>
<div class="doctools">
<h1 class="doctools_title">tcl-libraries(n) 4.99.20 manual &quot;NaviServer Manual&quot;</h1>
<div id="name" class="doctools_section"><h2><a name="name">Name</a></h2>
<p>tcl-libraries - NaviServer Tcl Libraries</p>
</div>
<div id="toc" class="doctools_section"><h2><a name="toc">Table Of Contents</a></h2>
<ul class="doctools_toc">
<li class="doctools_section"><a href="#toc">Table Of Contents</a></li>
<li class="doctools_section"><a href="#section1">Description</a></li>
<li class="doctools_section"><a href="#section2">What Are Tcl Libraries?</a></li>
<li class="doctools_section"><a href="#section3">When to Use Tcl Libraries</a></li>
<li class="doctools_section"><a href="#section4">Tcl Libraries</a></li>
<li class="doctools_section"><a href="#section5">Tcl Script Order of Evaluation</a></li>
<li class="doctools_section"><a href="#section6">Tcl-only Modules</a></li>
<li class="doctools_section"><a href="#see-also">See Also</a></li>
<li class="doctools_section"><a href="#keywords">Keywords</a></li>
</ul>
</div>
<div id="section1" class="doctools_section"><h2><a name="section1">Description</a></h2>
</div>
<div id="section2" class="doctools_section"><h2><a name="section2">What Are Tcl Libraries?</a></h2>
<p>A Tcl library is simply a directory containing Tcl scripts that are
sourced at startup by a virtual server. You can create private
libraries for individual virtual servers and public libraries that
affect all or some of an installation's virtual servers.</p>
<p>Each Tcl file in a library often contains one or more calls to
ns_register_proc, ns_schedule_proc, or ns_register_filter to bind a
script to a specific URL or URL hierarchy, plus the Tcl scripts that
will handle the URL(s). This example shows the ns_register_proc
function being used to bind the Tcl procedure <i class="term">hello</i> to handle a GET
request for /example/hello, plus the <i class="term">hello</i> procedure itself:</p>
<pre class="doctools_example">
 ns_register_proc GET /example/hello hello
 
 proc hello {} {
   ns_return 200 text/plain &quot;Hello World&quot;
 }
</pre>
<p>After the function is loaded (typically from a Tcl library directory)
and the server is started, one can test the function by visiting the URL</p>
<pre class="doctools_example">
 http://yourserver/example/hello
</pre>
<p>When NaviServer processes a method/URL request, it checks to see if
there is a Tcl script in the virtual server's private or shared
library to handle the method and URL. A private Tcl script registered
to handle a URL overrides a shared Tcl script registered to handle the
same URL.</p>
<p>Tcl libraries can also be created that contain no registration
functions; they may just contain Tcl functions that are called from
ADPs or from scheduled procedures.</p>
</div>
<div id="section3" class="doctools_section"><h2><a name="section3">When to Use Tcl Libraries</a></h2>
<p>The alternative to embedding Tcl scripts in HTML pages using ADPs (see
Chapter 2), is to store Tcl scripts in Tcl libraries. The situations
listed below are well-suited to the Tcl libraries approach.</p>
<dl class="doctools_definitions">
<dt>Inheritance:</dt>
<dd><p>If you want one Tcl script to handle a URL and all
 of its sub-URLs, it is better to store the script in a Tcl library
 and register it using ns_register_proc to handle a URL hierarchy.
 For example, you may want to manage a server domain name change by
 redirecting every response to the corresponding domain name on
 another server.</p></dd>
<dt>Special Extensions:</dt>
<dd><p>If you want one Tcl script to handle all files
 with a specific extension, like /*.csv, you would register the
 script with ns_register_proc to handle those files.</p></dd>
<dt>Scheduled Procedures:</dt>
<dd><p>If you want a Tcl script to be run at
 specific intervals, you can use the ns_schedule_* functions to run
 a script from the Tcl library at scheduled intervals. These
 procedures do not normally involve returning HTML pages and so are
 not well suited to ADPs.</p></dd>
<dt>Filters:</dt>
<dd><p>If you want a Tcl script to be called at
 pre-authorization, post-authorization, or trace time for a group
 of URLs, you would register a filter using the ns_register_filter
 function.</p></dd>
<dt>Re-using Tcl Scripts:</dt>
<dd><p>If there are Tcl scripts that you want to
 use in multiple situations, you can store them in a Tcl library
 and invoke them from within any ADP or Tcl script.</p></dd>
</dl>
</div>
<div id="section4" class="doctools_section"><h2><a name="section4">Tcl Libraries</a></h2>
<p>Tcl libraries contain Tcl files, which are loaded at startup time. The
functions defined in these Tcl files are available at run time without
any function loading overhead. 
NaviServer distinguishes between global (shared) and per-server
(private) library directories.</p>
<ul class="doctools_itemized">
<li><p>The <em>global (shared)</em> Tcl library directory is specified by the
parameter <i class="term">tcllibrary</i> in the <i class="term">ns/parameters</i> section and it
defaults to <i class="term">tcl</i> under NaviServer <i class="term">home</i> directory.</p>
<pre class="doctools_example">
 #
 # Global parameters
 #
 ns_section  ns/parameters {
     ns_param home        /usr/local/ns   ;# usual place
     ns_param tcllibrary  tcl             ;# default, full path: /usr/local/ns/tcl
 }
</pre>
</li>
<li><p>The <em>per-server (private)</em> Tcl library directory is
specified by the parameter <i class="term">library</i> in the
<i class="term">ns/server/$server/tcl</i> section.  It defaults to
<i class="term">modules/tcl</i>
under NaviServer <i class="term">home</i>.  One can specify a
different Tcl library directory for each server.</p>
<pre class="doctools_example">
 #
 # Global parameters
 #
 ns_section  ns/parameters {
     ns_param home        /usr/local/ns   ;# usual place
 }
 
 #
 # Parameters of the &quot;tcl&quot; section of the server &quot;myserver&quot;
 #
 ns_section ns/server/myserver/tcl {
     ns_param library     modules/tcl     ;# default, full path: /usr/local/ns/modules/tcl
 }
</pre>
</li>
</ul>
<p>Note that the specified directories need not reside under <i class="term">home</i>
(the NaviServer installation directory). Using a different directory
tree allows you to keep site-specific scripts physically separate from
the system-specific scripts supplied by NaviServer. For example,
OpenACS uses the per-server library directory to start the server with
the OpenACS specific packages and request/templating processing.</p>
</div>
<div id="section5" class="doctools_section"><h2><a name="section5">Tcl Script Order of Evaluation</a></h2>
<p>In general, the global (shared) Tcl libraries are loaded before the
per-server (private) libraries. This is true for the Tcl files placed
directly in the library directories as for the Tcl modules (more
details follow).
The Tcl library directories are a flat structure, from which all
contained Tcl files are loaded. Sometimes several Tcl files should be
handled together and might not be necessary for all managed
servers. For these purpose, Tcl-only modules can be used.</p>
<p>At server startup time, NaviServer initializes first the Tcl library
directories and then the specified Tcl-only modules:</p>
<ol class="doctools_enumerated">
<li><p>For the specified Tcl library directories, the <i class="term">init.tcl</i> file in
 that directory is sourced first (if it exists), and then all the
 remaining .tcl files are sourced in an alphabetical order.</p></li>
<li><p>For each module (including any Tcl-only modules) in the server:
 If a private Tcl directory is specified, the <i class="term">init.tcl</i> file in the
 module-name subdirectory of the private directory is sourced first
 (if it exists), and then all the remaining .tcl files are sourced
 alphabetically.</p></li>
</ol>
<p>If the <i class="term">tcl</i> section of the server configuration contains the
parameter <i class="term">initcmds</i> then these commands are executed after the
initialization of the Tcl-only modules. This feature is useful e.g.
for simple server configurations, where the full code of the
server-initialization can be put into the configuration file. So only one
file has to be maintained</p>
<pre class="doctools_example">
 #
 # Parameters of the &quot;tcl&quot; section of the server &quot;myserver&quot;
 #
 ns_section ns/server/myserver/tcl {
     ns_param initcmds {
         ns_log notice &quot;=== Hello World === server: [ns_info server]&quot;
     }
 }
</pre>
</div>
<div id="section6" class="doctools_section"><h2><a name="section6">Tcl-only Modules</a></h2>
<p>NaviServer supports C-based modules and Tcl-only modules. Tcl-only
modules are directories, containing potentially multiple Tcl source
files. When a Tcl-only module <i class="term">mymodule</i> is configured to be
loaded, during starutp NaviServer searches in the subdirectory of the
global (shared) library directory, and if not found the per-server
(privated) library directory for a directory with that name.  When it
is found the subdirectory <i class="term">mymodule</i> is initialized (as described
above).
To load a Tcl-only module named <i class="term">mymodule</i>, add the following
line to the per-server modules section in the configuration file:</p>
<pre class="doctools_example">
 #
 # Parameters of the &quot;modules&quot; section of the server &quot;myserver&quot;
 #
 ns_section ns/server/myserver/modules {
   ns_param mymodule Tcl
 }
</pre>
<p>Note that when Tcl modules are specified, only the named
subdirectories of the Tcl library directory are initialized (loaded).
Otherwise, all subdirectories of the Tcl library directories are
ignored. For example, if a server named <i class="term">myserver</i> has a Tcl
library directory defined as <i class="term">/home/mydir/tcl/myserver-lib</i>, and
the modules <i class="term">foo</i> and <i class="term">bar</i> are loaded,</p>
<pre class="doctools_example">
 #
 # Parameters of the &quot;tcl&quot; section of the server &quot;myserver&quot;
 #
 ns_section ns/server/myserver/tcl {
   ns_param library /home/mydir/tcl/myserver-lib
 }
 
 #
 # Parameters of the &quot;modules&quot; section of the server &quot;myserver&quot;
 #
 ns_section ns/server/myserver/modules {
   ns_param foo tcl
   ns_param bar tcl
 }
</pre>
<p>... then the following
directories will be initialized as server start-up:</p>
<pre class="doctools_example">
 /home/mydir/tcl/myserver-lib
 /home/mydir/tcl/myserver-lib/foo
 /home/mydir/tcl/myserver-lib/bar
</pre>
<p>Assume the library directory for module <i class="term">foo</i> contains these
files:</p>
<pre class="doctools_example">
 init.tcl
 aa.tcl
 zz.tcl
</pre>
<p>The Tcl files will be sourced in this order:</p>
<pre class="doctools_example">
 /home/mydir/tcl/myserver-lib/...
 /home/mydir/tcl/myserver-lib/foo/init.tcl
 /home/mydir/tcl/myserver-lib/foo/aa.tcl
 /home/mydir/tcl/myserver-lib/foo/zz.tcl
</pre>
<p>For a loaded Tcl-only module <i class="term">foo</i> the paths of the actual files
can by queried via <i class="term">ns_library</i> as shown in the following example.</p>
<pre class="doctools_example">
 set shared [ns_library shared foo]
 set priv [ns_library private foo]
</pre>
</div>
<div id="see-also" class="doctools_section"><h2><a name="see-also">See Also</a></h2>
<p>ns_config, ns_library</p>
</div>
<div id="keywords" class="doctools_section"><h2><a name="keywords">Keywords</a></h2>
<p><a href="../../index.html#configuration">configuration</a>, <a href="../../index.html#paths">paths</a>, <a href="../../index.html#resolution">resolution</a></p>
</div>
<div id="man-footer">
  
</div>
</div></body></html>
