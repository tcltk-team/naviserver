<!DOCTYPE html><html><head>
<title>c-driverdb - NaviServer Manual</title>
<link rel="stylesheet" href="../man.css" type="text/css">
</head>
<!-- Generated from file 'c-driverdb.man' by tcllib/doctools with format 'html'
   -->
<!-- c-driverdb.n
   -->
<body><div id="man-header">
  <a href="https://wiki.tcl-lang.org/page/NaviServer"><span class="logo">&nbsp;</span><strong>NaviServer</strong></a>
  - programmable web server
</div>
 <hr> [
   <a href="../../toc.html">Main Table Of Contents</a>
&#124; <a href="../toc.html">Table Of Contents</a>
&#124; <a href="../../index.html">Keyword Index</a>
 ] <hr>
<div class="doctools">
<h1 class="doctools_title">c-driverdb(n) 4.99.20 manual &quot;NaviServer Manual&quot;</h1>
<div id="name" class="doctools_section"><h2><a name="name">Name</a></h2>
<p>c-driverdb - NaviServer Database Driver Development Guide</p>
</div>
<div id="toc" class="doctools_section"><h2><a name="toc">Table Of Contents</a></h2>
<ul class="doctools_toc">
<li class="doctools_section"><a href="#toc">Table Of Contents</a></li>
<li class="doctools_section"><a href="#section1">Description</a></li>
<li class="doctools_section"><a href="#section2">Introduction</a></li>
<li class="doctools_section"><a href="#section3">Internal vs. External Database Drivers</a></li>
<li class="doctools_section"><a href="#section4">The Internal Driver API</a></li>
<li class="doctools_section"><a href="#section5">The External Driver API</a>
<ul>
<li class="doctools_subsection"><a href="#subsection1">Functions to be Implemented</a></li>
<li class="doctools_subsection"><a href="#subsection2">Utility Functions</a></li>
<li class="doctools_subsection"><a href="#subsection3">Constants</a></ul>
</li>
</ul>
</div>
<div id="section1" class="doctools_section"><h2><a name="section1">Description</a></h2>
</div>
<div id="section2" class="doctools_section"><h2><a name="section2">Introduction</a></h2>
<p>A database driver is a module that interfaces between the NaviServer
database-independent nsdb API and the API of a particular DBMS. A
database driver provides implementations of a standard set of
functions for doing such things as opening connections to the
database, sending SQL statements, returning results to a standard
form, and closing connections. NaviServer takes care of managing
collections of open database connections called database pools.</p>
<p>This manual page is kept of as a reference. Part of this is outdated.</p>
<p>Database driver modules look much like ordinary NaviServer modules but
are loaded differently. Instead of being listed with other modules in
the ns/server/server-name/modules section of the configuration file, a
database driver is listed in the ns/db/drivers section -- a database
driver is not tied to a particular virtual server. The database driver
initialization function must call Ns_DbRegisterDriver() with an array
of pointers to functions. These functions are then later used by
NaviServer to perform database operations. The virtual server
initialization function is called each time nsdb is loaded into a
virtual server. The server initialization function
(Ns_dbms-nameServerInit) adds the Tcl command Ns_dbms-name to the
server's Tcl interpreters. The Ns_dbms-name command can then be used
to fetch driver-specific information about an active connection.</p>
<p>NOTE: The above naming scheme is simply based on convention. It is not
enforced, and can be changed at the programmer's discretion.</p>
</div>
<div id="section3" class="doctools_section"><h2><a name="section3">Internal vs. External Database Drivers</a></h2>
<p>Before developing a database driver, you need to decide whether the
driver will be internal or external:</p>
<ul class="doctools_itemized">
<li><p>Internal: An internal driver is tightly coupled with the
     NaviServer; the database client libraries are linked directly into
     the server.</p></li>
<li><p>External: An external driver is loosely coupled; the nsext driver
     sends messages to an external database proxy daemon instead of
     calling the database client libraries directly. This database
     proxy daemon can be a local or remote process.</p></li>
</ul>
<p>While an internal driver can be faster and can use less resources than
an external driver, there are several reasons to develop an external
driver:</p>
<ul class="doctools_itemized">
<li><p>Many database client libraries are not thread-safe -- you'll need
     to be very careful about locking the libraries to use them in an
     internal driver, and this can unnecessarily slow down simultaneous
     queries.</p></li>
<li><p>Many database client libraries make assumptions regarding
     per-process resources such as signals.</p></li>
<li><p>An external driver can be used to access a database from a
     platform that may not be supported by the database client library.</p></li>
</ul>
</div>
<div id="section4" class="doctools_section"><h2><a name="section4">The Internal Driver API</a></h2>
<p>The set of function points that a database driver implements and what
NaviServer expects is as follows:</p>
<ol class="doctools_enumerated">
<li><p>Name</p></li>
<li><p>Database type</p></li>
<li><p>Server initialization</p></li>
<li><p>Open a database connection</p></li>
<li><p>Close a database connection</p></li>
<li><p>Get row from table</p></li>
<li><p>Flush</p></li>
<li><p>Cancel a database connection</p></li>
<li><p>Get information about a table</p></li>
<li><p>Get list of tables</p></li>
<li><p>Best row identifier (identifies each row uniquely)</p></li>
<li><p>Execute SQL query</p></li>
<li><p>Optional: Reset a database handle when it gets checked back into
    the pool.</p></li>
</ol>
<p>The database driver's job is to make the appropriate DBMS-specific
function calls that will implement these functions. The driver must
provide an implementation of the function NsDbDriverInit(); This
function is executed once, when the server starts up. The most
important thing it does is call Ns_DbRegisterDriver() with an array of
all the functions implemented by the driver. Here's an example from
the PostgreSQL driver (nsdbpg, simplified):</p>
<pre class="doctools_example">
 static Ns_DbProcPgProcs = {
    {DbFn_DbType,       (ns_funcptr_t)DbType},
    {DbFn_Name,         (ns_funcptr_t)DbType},
    {DbFn_OpenDb,       (ns_funcptr_t)OpenDb},
    {DbFn_CloseDb,      (ns_funcptr_t)CloseDb},
    {DbFn_BindRow,      (ns_funcptr_t)BindRow},
    {DbFn_Exec,         (ns_funcptr_t)Exec},
    {DbFn_GetRow,       (ns_funcptr_t)GetRow},
    {DbFn_GetRowCount,  (ns_funcptr_t)GetRowCount},
    {DbFn_Flush,        (ns_funcptr_t)Flush},
    {DbFn_Cancel,       (ns_funcptr_t)Flush},
    {DbFn_ResetHandle,  (ns_funcptr_t)ResetHandle},
    {DbFn_ServerInit,   (ns_funcptr_t)Ns_PgServerInit},
    {DbFn_End, NULL}
 };
 
 NS_EXPORT Ns_ReturnCode
 Ns_DbDriverInit(const char *driver, const char *configPath)
 {
    Ns_ReturnCode status;
    /*
     * Register the PostgreSQL driver functions with nsdb.
     * Nsdb will later call the Ns_PgServerInit() function
     * for each virtual server which utilizes nsdb.
     */
    status = Ns_DbRegisterDriver(driver, &amp;procs[0]);
    if (status) != NS_OK) {
        Ns_Log(Error, &quot;Ns_DbDriverInit(%s):  Could not register the %s driver.&quot;,
            driver, pgDbName);
        return NS_ERROR;
    }
    Ns_Log(Notice, &quot;%s loaded.&quot;, pgDbName);
    return status;
 }
</pre>
<p>In more detail, here's what each of these functions needs to do:</p>
<ol class="doctools_enumerated">
<li><p>const char *Ns_dbms-nameName(Ns_DbHandle *handle);</p>
<p>This function returns the string which identifies the database
             driver.</p></li>
<li><p>const char *Ns_dbms-nameDbType(Ns_DbHandle *handle);</p>
<p>This function returns the string which identifies the database
             type. Usually it is the same as the name of the driver.</p></li>
<li><p>Ns_ReturnCode Ns_dbms-nameServerInit(const char *hServer, const
char *hModule, const char *driver);</p>
<p>Ns_dbms-nameServerInit calls another function named
             Ns_dbms_nameInterpInit which is responsible for adding
             the command Ns_dbms-name to a single Tcl interpreter.
             Ns_dbms-nameServerInit calls Ns_dbms-nameInterpInit for
             each interpreter in the virtual server that is being
             initialized.</p></li>
<li><p>Ns_ReturnCode Ns_dbms-nameOpenDb(Ns_DbHandle *dbh);</p>
<p>This function takes a pointer (typically known as the &quot;handle&quot;)
             to the Ns_DbHandle structure as an argument. The handle
             contains information such as the driver name, name of the
             datasource, username and password, name of the database
             pool and some other parameters. The structure is as
             follows:</p>
<pre class="doctools_example">
 typedef struct Ns_DbHandle {
    const char *driver;
    const char *datasource;
    const char *user;
    const char *password;
    void       *connection;
    const char *poolname; /* poolname associated for this handle */
    bool        connected;
    bool        verbose; /* Was previously used for general verbosity, then unused, now verboseError */
    Ns_Set     *row;     /* used for row data during binds, getrows, etc.*/
                         /* used by internal Db API */
    char        cExceptionCode[6];
    Ns_DString  dsExceptionMsg;
    void       *context;
    void       *statement; /* used by ODBC driver statements */
    bool        fetchingRows;
 } Ns_DbHandle;
</pre>
<p>The statement field is used as a pointer to your allocated
             statement handles (hstmt). It can be used to pass a
             DbHandle with a pointer to the statement handle to other
             driver functions to invoke cancels (e.g., SQLCancel())
             and error reporting (e.g., SQLError()).</p>
<p>This function takes the information contained in the handle and
             opens a connection to the database named in the handle
             along with the username and password. If a connection is
             successfully established, then OpenDb performs some
             database specific functions, and returns NS_OK. If it is
             unable to establish a connection to the database server,
             or if it is unable to perform the required database
             specific functions, it returns NS_ERROR. The status is
             logged by using the Ns_Log function.</p>
<p>Note: For more information about database-specific functions, the
             documentation about its API should be consulted.</p></li>
<li><p>Ns_ReturnCode Ns_dbms-nameCloseDb(Ns_DbHandle *handle);</p>
<p>This function takes a handle as an argument and terminates the
             connection to the database server. It also cleans up the
             handle parameters. It returns with either NS_OK or
             NS_ERROR (in case CloseDb fails to close the connection).</p></li>
<li><p>int Ns_dbms-nameGetRow(const Ns_DbHandle *handle, const Ns_Set *row);</p>
<p>The results of a SELECT query are usually a number of rows.
             Ns_dbms-nameGetRow is responsible to obtain these rows
             from the database and return them to NaviServer for
             display/manipulation. This function typically does the
             following:</p>
<p>checks if it is being called from within a fetch row loop
             checks if the end of data is reached, and if so, stops
             returns the next row in the result set</p>
<p>You can return the values something like this:</p>
<p>Ns_SetPutValue(row, (int) i, colval);</p>
<p>This function should return either NS_ERROR or NS_OK.</p></li>
<li><p>Ns_ReturnCode Ns_dbms-nameFlush(Ns_DbHandle *handle);</p>
<p>This function flushes any rows which are waiting to be retrieved
             after a SELECT is executed. The rows are irretrievably
             lost. The fields for columns, tuples, and current tuple
             in the connection structure are reset to 0. The result
             field is reset to NULL. This function should return
             either NS_ERROR or NS_OK.</p></li>
<li><p>Ns_ReturnCode Ns_dbms-nameCancel(Ns_DbHandle *handle);</p>
<p>A call to this function results in cancelling that particular
             database connection.</p></li>
<li><p>int Ns_dbms-nameExec(Ns_DbHandle *handle, const char *sql);</p>
<p>This function takes an SQL command and sends it to the DBMS. If
             the command is returns rows, the function should return
             NS_ROWS. If the command was DDL or DML, then the function
             should return NS_DML. And of course if there is an error
             executing the SQL, the function should return NS_ERROR.
             It is recommended that you define one Ns_dbms-nameExec
             procedure that handles both queries that return rows and
             those that do not. When Ns_DbExec is invoked in
             NaviServer, it calls whatever Ns_DbExec function is
             defined. When Ns_DbSelect is invoked, it tries Ns_DbExec
             first and then Ns_DbSelect.</p></li>
<li><p>Ns_ReturnCode Ns_dbms-nameResetHandle(Ns_DbHandle *handle);</p>
<p>This function will be called with the database handle every time
             one is returned to the database pool. You can use this to
             normalize its state for the next use; for example always
             setting the handle to autocommit mode and aborting any
             uncommitted transactions.</p></li>
</ol>
</div>
<div id="section5" class="doctools_section"><h2><a name="section5">The External Driver API</a></h2>
<p>To build an external driver, you need to provide implementations for
several functions, and then link your code with the provided
'nspdmain' program, found in the nspd.a library. The resulting
&quot;database proxy daemon&quot; is contacted by the server's &quot;external&quot; driver
(the client in the following descriptions) for all database
activities. The 'nspdmain' program handles all communication with the
server's 'external' driver, calling your routines database-specific
functions. Note that there is a single database proxy daemon
associated with each database connection, so all of the state for each
connection is encapsulated here. Database connections are managed
efficiently by the server's database pool mechanism.</p>
<p>The next section summarizes the External Driver Proxy Daemon
functions. The reference section at the end of this appendix describes
each function in detail and gives a pseudo-code implementation. Each
implementation of a function may be a different for each
dbms-specification. Consult your database client library documentation
and the freely distributed NaviServer examples of the Sybase Proxy
daemons for more information and dbms-specific examples.</p>
<div id="subsection1" class="doctools_subsection"><h3><a name="subsection1">Functions to be Implemented</a></h3>
<p>The set of functions that need to be implemented are:</p>
<ol class="doctools_enumerated">
<li><p>void *Ns_PdDbInit (void)</p>
<p>This function is called once from the 'nspdmain' when the
             database driver is initialized at process startup. It
             normally allocates and returns a dbms-specific structure
             to be passed to all other routines. It also calls any
             dbms-specific initialization routines. This function does
             not open a connection to the database.</p></li>
<li><p>void Ns_PdDbCleanup(void *handle)</p>
<p>This function is called once from the 'nspdmain' when the
             database driver is initialized at process startup. It
             performs cleanup associated with process shutdown and
             frees the database-specific handle.</p></li>
<li><p>void Ns_PdDbOpen(void *handle, char *openArgs)</p>
<p>This function opens a connection to the database based on the
             openArgs passed in. The handle passed in is the handle
             that was returned by Ns_PdDbInit, and will usually be
             cast to some driver-specific data structure. If the open
             operation is successful, this function should call
             Ns_PdSendString with OK_STATUS. On failure, the function
             should use Ns_PdSendString to return an error string.</p></li>
<li><p>void Ns_PdDbClose(void *handle)</p>
<p>This function closes the database. It should not free the
             database handle. If the close operation is successful,
             this function should call Ns_PdSendString with OK_STATUS.
             On failure, the function should use Ns_PdSendString to
             return an error string.</p></li>
<li><p>void Ns_PdDbExec(void *handle, const char *sql)</p>
<p>This function executes a SQL query. If the SQL is executed
             successfully, this function should call Ns_PdSendString
             with OK_STATUS followed by either
             Ns_PdSendString(EXEC_RET_ROWS) or
             Ns_PdSendString(EXEC_RET_DML), depending on whether the
             SQL returned rows or was a DML statement. On failure, the
             function should use Ns_PdSendException to return an
             error.</p></li>
<li><p>void Ns_PdDbFlush(void *handle)</p>
<p>Flushes any pending data. Usually this function will call
             Ns_PdDbCancel or something similar along with any other
             database specific clean-up routines. If the flush
             operation is successful, this function should call
             Ns_PdSendString with an OK_STATUS. On failure, the
             function should use Ns_PdSendString to return an error
             string.</p></li>
<li><p>void Ns_PdDbCancel(void *handle)</p>
<p>This function cancels the current database operation. If the
             cancel operation was successful, this function should
             call Ns_PdSendString with OK_STATUS. On failure, the
             function should use Ns_PdSendString to return an error
             string.</p></li>
<li><p>void Ns_PdDbBindRow (void *handle)</p>
<p>This function should retrieve from the DBMS a list of column
             names of rows and return this data to the client. If the
             bind-row operation is successful, this function should
             call Ns_PdSendString with an OK_STATUS. On failure this
             function should return an exception code with
             Ns_PdSendException.</p></li>
<li><p>void Ns_PdDbGetRow(void *handle, char *columnCount)</p>
<p>This function should retrieve from the DBMS the row associated
             with the columnCount column and send this data to the
             client. If the get-row operation was successful, this
             function should call Ns_PdSendString with an OK_STATUS
             and then send the data with Ns_PdDbSendData. On failure,
             the function should use Ns_PdSendException to return an
             error.</p></li>
<li><p>void Ns_PdDbTableList (void *handle, char *includeSystem)</p>
<p>This function should retrieve the list of table names from the
             database associated with the handle and send this data to
             the client. The includeSystem parameter indicates whether
             to include system tables with this list. The function
             should implement the following protocol:</p>
<p>If the initial SQL query returns a successful status of DB_ROWS
             when generating the table names, then this function
             should:</p>
<ol class="doctools_enumerated">
       
<li><p>Call Ns_PdSendString with an OK_STATUS.</p></li>
<li><p>Call Ns_PdSendString with a size of the item (table name).</p></li>
<li><p>Call Ns_PdSendData to send the actual data associated with
         the name and to signal the end of data.</p></li>
<li><p>If an exception occurs during the processing of the table
         data, then this function should send the partial data to the
         client and indicate an error with Ns_PdDbLog.</p></li>
</ol>
<p>If an exception is raised before successfully retrieving some of
             the table data, then this function should call
             Ns_PdSendException to return an error.</p></li>
<li><p>void Ns_PdDbGetTableInfo(void *handle, char *tableName)</p>
<p>This function should retrieve the system catalog information
             (columns, types, etc.) about a table and send this to the
             client.</p>
<p>If the initial SQL select query returns a successful status of
             DB_ROWS, then this function should:</p>
<ol class="doctools_enumerated">
       
<li><p>Call Ns_PdSendString with an OK_STATUS.</p></li>
<li><p>Call Ns_PdSendRowInfo to send to the client the column info.</p></li>
<li><p>Call Ns_PdSendRowInfo to send to the client subsequent row
         info.</p></li>
<li><p>Call Ns_PdSendData to indicate to the client that END_DATA
         has been reached.</p></li>
<li><p>If an exception occurs during the processing of the row info,
         then this function should send the partial data to the client
         and indicate an error with Ns_PdDbLog.</p></li>
</ol>
<p>If an exception is raised before successfully retrieving some of
             the row info, then this function should call
             Ns_PdSendException to return an error.</p></li>
<li><p>void Ns_PdDbBestRowId(void *handle, char *tableName)</p>
<p>This function retrieves the primary key of a table from the
             database and sends this to the client. If a table has a
             primary key, NaviServer can perform row updates and
             deletes. If the best-row-id operation is successful, this
             function should call Ns_PdSendString with an OK_STATUS.
             If a best-row-id is found then call Ns_PdSendString with
             the column name; otherwise, call Ns_PdSendString with
             NO_BESTROWID. On failure this function should return an
             exception code with Ns_PdSendException.</p></li>
<li><p>void Ns_PdDbIndentify(void *handle)</p>
<p>This function sends a string identifying the proxy daemon name
             and version with Ns_PdSendString.</p></li>
<li><p>void Ns_PdDbGetTypes(void *handle)</p>
<p>This function sends a string of the data types for the database
             with Ns_PdSendString.</p></li>
<li><p>void Ns_PdDbResultId(void *handle)</p>
<p>This function sends to the client with Ns_PdSendString the id of
             the last object affected by an exec command or a null
             terminated string.</p></li>
<li><p>void Ns_PdDbResultRows(void *handle)</p>
<p>This function sends to the client with Ns_PdSendString the number
             of rows affected by last exec command.</p></li>
<li><p>void Ns_PdDbSetMaxRows(void *handle, char *maxRows)</p>
<p>This function sets the max rows for the database. If this
             function is successful or the SQL command is undefined
             for your specific DBMS, it should call Ns_PdSendString
             with an OK_STATUS. If set-max-rows is defined for your
             specific DBMS and there is an exception raised, then this
             function should send an exception code with
             Ns_PdSendException.</p></li>
<li><p>void Ns_PdDbSpExec(void *handle)</p>
<p>This function executes a stored procedure that has been
             initialized with Ns_PdDbSpStart and Ns_PdDbSpSetParam.</p></li>
<li><p>void Ns_PdDbSpGetParams(void *handle)</p>
<p>This function returns output parameters from a previously
             executed stored procedure.</p></li>
<li><p>void Ns_PdDbSpReturnCode (void *handle)</p>
<p>This function gets the return code from a previously executed
             stored procedure.</p></li>
<li><p>void Ns_PdDbSpSetParam (void *handle, char *args)</p>
<p>This function sets parameters in a stored procedure before
             executing it.</p></li>
<li><p>void Ns_PdDbSpStart(void *handle, char *procname)</p>
<p>This function begins the definition of a stored procedure.</p></li>
</ol>
</div>
<div id="subsection2" class="doctools_subsection"><h3><a name="subsection2">Utility Functions</a></h3>
<p>In implementing the above functions, you will need to call some of the
various utility functions described below. When using these functions,
do not pass a parameter of NULL for string values. If you do, an error
message is printed to the syslog and the function returns.</p>
<ol class="doctools_enumerated">
<li><p>void Ns_PdLog(Ns_PdLogMsgType errtype, char *fmt, ...)</p>
<p>The Ns_PdLog function sends a formatted messages to the client.
             Allowable values for the log type are the following with
             a default type of Error: Notice, Trace, Error.</p></li>
<li><p>void Ns_PdSendString(char *string)</p>
<p>This function sends a string to the client.</p></li>
<li><p>void Ns_PdSendException(char *code, char *msg)</p>
<p>The command sends an exception code and an exception message to the
             client.</p></li>
<li><p>Ns_PdParseOpenArgs(char *openargs, char **datasource, char **user,
    char **password, char **param)</p>
<p>This function parses the datasource, user, password, and param
             parameters leaving missing elements as NULL. This
             function is normally called from within Ns_PdDbOpen.</p></li>
<li><p>void Ns_PdSendRowInfo(Ns_PdRowInfo * rowInfo)</p>
<p>This function sends a row encapsulated in an Ns_PdRowInfo
             structure to the client.</p></li>
<li><p>void Ns_PdSendData(char *data, int len)</p>
<p>This function sends data of length len to the client. You
             indicate that you are finished sending data by calling
             this function with END_DATA.</p></li>
<li><p>void Ns_PdFreeRowInfo(Ns_PdRowInfo * rowInfo, int fFreeData)</p>
<p>This function frees an ns_PdRowInfo data structure. If fFreeData
             is a nonzero value, then this function frees the data
             associated with the ns_PdRowData structure (encapsulated
             in Ns_PdRowInfo) as well.</p></li>
</ol>
</div>
<div id="subsection3" class="doctools_subsection"><h3><a name="subsection3">Constants</a></h3>
<p>The following constants are used in the example drivers:</p>
<p>#define EXCEPTION_CODE_MAX 32</p>
<p>#define EXCEPTION_MSG_MAX 4096</p>
<p>These constants are used for the buffer sizes for the database
error/exception code and error/exception message, respectively. The
given buffer size must include a terminating null byte. In other
words, you can have an exception message of up to 4095 bytes,
reserving the last byte for a NUL byte.</p>
</div>
</div>
<div id="man-footer">
  
</div>
</div></body></html>
