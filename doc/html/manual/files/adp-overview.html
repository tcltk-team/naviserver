<!DOCTYPE html><html><head>
<title>adp-overview - NaviServer Manual</title>
<link rel="stylesheet" href="../man.css" type="text/css">
</head>
<!-- Generated from file 'adp-overview.man' by tcllib/doctools with format 'html'
   -->
<!-- adp-overview.n
   -->
<body><div id="man-header">
  <a href="https://wiki.tcl-lang.org/page/NaviServer"><span class="logo">&nbsp;</span><strong>NaviServer</strong></a>
  - programmable web server
</div>
 <hr> [
   <a href="../../toc.html">Main Table Of Contents</a>
&#124; <a href="../toc.html">Table Of Contents</a>
&#124; <a href="../../index.html">Keyword Index</a>
 ] <hr>
<div class="doctools">
<h1 class="doctools_title">adp-overview(n) 4.99.20 manual &quot;NaviServer Manual&quot;</h1>
<div id="name" class="doctools_section"><h2><a name="name">Name</a></h2>
<p>adp-overview - NaviServer ADP Development</p>
</div>
<div id="toc" class="doctools_section"><h2><a name="toc">Table Of Contents</a></h2>
<ul class="doctools_toc">
<li class="doctools_section"><a href="#toc">Table Of Contents</a></li>
<li class="doctools_section"><a href="#section1">Description</a></li>
<li class="doctools_section"><a href="#section2">What Are ADPs?</a></li>
<li class="doctools_section"><a href="#section3">When to Use ADPs</a></li>
<li class="doctools_section"><a href="#section4">When to Use Tcl Libraries</a></li>
<li class="doctools_section"><a href="#section5">Configuring ADP Processing</a>
<ul>
<li class="doctools_subsection"><a href="#subsection1">Required Configuration Parameters</a></li>
<li class="doctools_subsection"><a href="#subsection2">List of Configuration Parameters</a></li>
</ul>
</li>
<li class="doctools_section"><a href="#section6">Building ADPs</a></li>
<li class="doctools_section"><a href="#section7">Debugging ADPs with TclPro</a></li>
<li class="doctools_section"><a href="#section8">ADP Syntax</a></li>
<li class="doctools_section"><a href="#section9">Registered ADP Tags</a></li>
<li class="doctools_section"><a href="#section10">Example ADPs</a>
<ul>
<li class="doctools_subsection"><a href="#subsection3">Example 1: Return partial HTML page conditionally</a></li>
<li class="doctools_subsection"><a href="#subsection4">Example 2: Return full HTML page conditionally</a></li>
<li class="doctools_subsection"><a href="#subsection5">Example 3: Return information from the database</a></li>
<li class="doctools_subsection"><a href="#subsection6">Example 4: Get form information and insert into the database</a></li>
<li class="doctools_subsection"><a href="#subsection7">Example 5: ADP sampler with includes, recursion, and streaming</a></li>
</ul>
</li>
<li class="doctools_section"><a href="#see-also">See Also</a></li>
<li class="doctools_section"><a href="#keywords">Keywords</a></li>
</ul>
</div>
<div id="section1" class="doctools_section"><h2><a name="section1">Description</a></h2>
</div>
<div id="section2" class="doctools_section"><h2><a name="section2">What Are ADPs?</a></h2>
<p>Probably the easiest way to make NaviServer output dynamic content is
to embed a Tcl script in an HTML page with NaviServer Dynamic Pages
(ADPs). ADPs are HTML pages that are parsed and run on the server when
the page is accessed. ADPs contain HTML tags and Tcl scripts that are
embedded within the HTML tags. The embedded Tcl scripts can call other
Tcl scripts that reside in separate files, allowing you to reuse Tcl
code.</p>
</div>
<div id="section3" class="doctools_section"><h2><a name="section3">When to Use ADPs</a></h2>
<p>ADPs are ideal in situations where you want to generate all or part of
a specific page dynamically. You can re-use code by storing Tcl
scripts in Tcl libraries and invoking them from within multiple ADPs.
You can also include files and parse other ADPs from within your ADPs.</p>
<p>Here are some examples of applications you can use ADPs for:</p>
<ul class="doctools_itemized">
<li><p>Returning HTML conditionally</p></li>
<li><p>Retrieving information from a database to use in a page</p></li>
<li><p>Inserting information from a form into a database</p></li>
</ul>
</div>
<div id="section4" class="doctools_section"><h2><a name="section4">When to Use Tcl Libraries</a></h2>
<p>The alternative to embedding Tcl scripts in HTML pages using ADPs, is
to store Tcl scripts in Tcl libraries and register them to handle
specific URLs or URL hierarchies. There are some situations, such as
those listed below, that are better suited to the Tcl libraries
approach.</p>
<ul class="doctools_itemized">
<li><p>Inheritance: If you want one Tcl script to handle a URL and all
 of its sub-URLs, it is better to store the script in a Tcl library
 and register it using ns_register_proc to handle a URL hierarchy.
 For example, you may want to manage a server domain name change by
 redirecting every response to the corresponding domain name on
 another server.</p></li>
<li><p>Special Extensions: If you want one Tcl script to handle all
 files with a specific extension, like /*.csv, you would register
 the script with ns_register_proc to handle those files.</p></li>
<li><p>Scheduled Procedures: If you want a Tcl script to be run at
 specific intervals, you can use the ns_schedule_* functions to run
 a script from the Tcl library at scheduled intervals. These
 procedures do not normally involve returning HTML pages and so are
 not well suited to ADPs.</p></li>
<li><p>Filters: If you want a Tcl script to be called at
 pre-authorization, post-authorization, or trace time for a group
 of URLs, you would register a filter using the ns_register_filter
 function.</p></li>
<li><p>Re-using Tcl Scripts: If there are Tcl scripts that you want to
 use in multiple situations, you can store them in a Tcl library
 and invoke them from within any ADP or Tcl script.</p></li>
</ul>
</div>
<div id="section5" class="doctools_section"><h2><a name="section5">Configuring ADP Processing</a></h2>
<p>Since you will be creating HTML pages that contain Tcl scripts, you
will need to specify which pages the server will need to parse for Tcl
commands and process.</p>
<div id="subsection1" class="doctools_subsection"><h3><a name="subsection1">Required Configuration Parameters</a></h3>
<ul class="doctools_itemized">
<li><p>Use the Map configuration parameter to determine
 which files are to be processed. For example, you can specify that
 all files with the .adp extension are to be processed by the
 server. Or, you can specify that all files that reside in a
 certain directory (for example, /usr/pages/adp) will be processed.</p></li>
</ul>
</div>
<div id="subsection2" class="doctools_subsection"><h3><a name="subsection2">List of Configuration Parameters</a></h3>
<p>The following table describes all the parameters that can be set
within the ADP section of the configuration file:</p>
<p>Section <em>ns/server/$server/adp</em>:</p>
<ul class="doctools_itemized">
<li><p><em>Cache</em> - If set to on, ADP caching is enabled, Default: on</p></li>
<li><p><em>DebugInit</em> - The procedure to run when debugging begins,
         Default: ns_adp_debuginit</p></li>
<li><p><em>EnableExpire</em> - If set to on, the &quot;Expires: now&quot; header is set on all
         outgoing ADPs, Default: off</p></li>
<li><p><em>EnableCompress</em> - If set to on, extraneous spaces within an HTML page are removed,
         Default: off</p></li>
<li><p><em>EnableDebug</em> - If set to on, appending &quot;?debug&quot; to a URL will enable TclPro debugging,
         Default: off</p></li>
<li><p><em>Map</em> - The Map parameter specifies which pages the server will parse. You can
         specify a file extension (such as /*.adp) or a directory (such as
         /usr/pages/adp). If no directory is specified, the pages directory is
         assumed. The wildcards * ? and  can be included in the Map
         specification. You can specify multiple Map settings.</p>
<p>The following example specifies that all files in the pages directory
with the extensions .adp or .asp will be parsed, and all files in the
/usr/pages/myadps directory will be parsed.</p>
<pre class="doctools_example">
 Map &quot;/*.adp&quot;
 Map &quot;/*.asp&quot;
 Map &quot;/usr/pages/myadps&quot;
</pre>
</li>
<li><p><em>StartPage</em>  - The file to be run on every connection instead of the requested ADP.
     It can be used to perform routine initialization. It would then
     usually include the requested ADP by calling:</p>
<pre class="doctools_example">
 ns_adp_include [ns_adp_argv 0]
</pre>
</li>
</ul>
</div>
</div>
<div id="section6" class="doctools_section"><h2><a name="section6">Building ADPs</a></h2>
<ol class="doctools_enumerated">
<li><p>Create an HTML page. Use an HTML editor or a file editor to create
 an HTML page. Be sure to either name the file with the correct
 extension or save it in the correct directory on your server as
 specified by the Map parameter setting.</p>
<p>If you plan to use the Tcl script to return part or all of the
 page's content, just omit that part of the page, but you
 can create all of the surrounding HTML.</p></li>
<li><p>Add your Tcl scripts with a file editor. Insert your Tcl scripts
 in the HTML file where you want them to be processed. Be sure to
 enclose each Tcl script using one of the &lt;SCRIPT&gt; or &lt;% ...%&gt;
 syntaxes . Save the HTML file.</p></li>
<li><p>View the HTML page in a browser. Visit the page you have created
 in a browser to see if the Tcl scripts work correctly. If you have
 set the EnableDebug parameter, you can append &quot;?debug&quot; to the URL
 to enable TclPro debugging.</p></li>
<li><p>Continue editing and viewing until it works correctly. Continue
 editing the page in a file editor, saving it, and refreshing it in
 a browser until it works the way you want it to.</p></li>
</ol>
</div>
<div id="section7" class="doctools_section"><h2><a name="section7">Debugging ADPs with TclPro</a></h2>
<p>To debug your ADPs with TclPro, follow these steps:</p>
<ul class="doctools_itemized">
<li><p>Download and install TclPro from http://www.scriptics.com/tclpro/.
 Temporary licenses are available.</p></li>
<li><p>Copy the prodebug.tcl file from the TclPro distribution to the
 modules/tcl directory.</p></li>
<li><p>Set the EnableDebug parameter in the ns/server/$server/adp
 section of the configuration file to On.</p></li>
<li><p>Run TclPro and start a &quot;remote project&quot;, which puts TclPro into a
 mode waiting for NaviServer to connect.</p></li>
<li><p>Open an ADP file with the ?debug=&lt;pattern&gt; query string in
 addition to any other query data you may send. NaviServer will then
 trap on ADP files matching the pattern. For example, you can just
 use debug=*.adp to trap all files, or you can use
 debug=someinclude.inc file to trap in an included file.</p></li>
</ul>
<p>To set a breakpoint in a procedure you'll have to &quot;instrument&quot;
 the procedure either after the debugger traps the first
 time or with the &quot;dproc=&lt;pattern&gt;&quot; query argument.</p>
</div>
<div id="section8" class="doctools_section"><h2><a name="section8">ADP Syntax</a></h2>
<p>There are three different syntaxes you can use to embed a Tcl script
into an HTML page. Not all syntaxes are available with all ADP
parsers. You must be using the appropriate ADP parser to process a
specific syntax.</p>
<p>Insert Tcl commands between any of the following sets of HTML tags:</p>
<ul class="doctools_itemized">
<li><p>&lt;script language=&quot;tcl&quot; runat=&quot;server&quot; stream=&quot;on&quot;&gt;</p>
<pre class="doctools_example">
 &lt;script language=&quot;tcl&quot; runat=&quot;server&quot; stream=&quot;on&quot;&gt;
 ...
 &lt;/script&gt;
</pre>
<p>The contents of the script are interpreted using the Tcl
interpreter. The result is not inserted into the page, however. You
can use the <b class="cmd">ns_adp_puts</b> Tcl function to put content into the page.</p>
<p>The language=tcl attribute is optional. Future enhancements to ADPs
will include the capability to embed scripts in other scripting
languages, and you will be able to configure the default language. If
the language attribute is set to anything except tcl, the script will
not be processed, and a warning will be written to the log file.</p>
<p>The runat=&quot;server&quot; attribute is required. If this attribute is missing,
the script will not be processed. The runat=&quot;server&quot; attribute is
necessary so that client-side scripts are not executed accidentally.</p>
<p>The stream=on attribute is optional. If it is included, all output for
the rest of the page is streamed out to the browser as soon as it is
ready. Streaming is useful when your script may take a long time to
complete (such as a complex database query). Content is output to the
page gradually as the script is running. One disadvantage of streaming
is that the server cannot return a Content-length header with the
response. Content-length can speed up the connection, especially if
the connection is going through a proxy server.</p></li>
<li><p>&lt;% ... %&gt;</p>
<p>This syntax is evaluated exactly the same as the first syntax, above,
except that you cannot specify any of the attributes. The language=Tcl
and runat=&quot;server&quot; attributes are implied. Streaming is not allowed with
this syntax, but output within this syntax will be streamed if
streaming was turned on in a previous script. This syntax can be used
inside HTML tags.</p></li>
<li><p>&lt;%= ... %&gt;</p>
<p>The Tcl commands within these tags are evaluated as the argument to an
<b class="cmd">ns_adp_puts</b> command, which inserts the results into the page. This syntax
can be used inside HTML tags.</p></li>
</ul>
</div>
<div id="section9" class="doctools_section"><h2><a name="section9">Registered ADP Tags</a></h2>
<p>You can define your own ADP tags with the ns_adp_register* Tcl functions.</p>
</div>
<div id="section10" class="doctools_section"><h2><a name="section10">Example ADPs</a></h2>
<p>This section contains the following ADP examples:</p>
<ul class="doctools_itemized">
<li><p>Example 1: Return partial HTML page conditionally</p></li>
<li><p>Example 2: Return full HTML page conditionally</p></li>
<li><p>Example 3: Return information from the database</p></li>
<li><p>Example 4: Get form information and insert into the database</p></li>
<li><p>Example 5: ADP sampler with includes, recursion, and streaming</p></li>
</ul>
<div id="subsection3" class="doctools_subsection"><h3><a name="subsection3">Example 1: Return partial HTML page conditionally</a></h3>
<p>This ADP example tests for various browsers and returns a different message in each case.</p>
<pre class="doctools_example">
 &lt;%
 ns_adp_puts &quot;Hello&quot;
 set ua [ns_set get [ns_conn headers] &quot;user-agent&quot;]
 ns_adp_puts &quot;$ua &quot;
 
 if [string match &quot;*MSIE*&quot; $ua] {
    ns_adp_puts &quot;This is MS Internet Explorer&quot;
 } elseif [string match &quot;*Mozilla*&quot; $ua] {
    ns_adp_puts &quot;This is Netscape or a Netscape compatible browser&quot;
 } else {
    ns_adp_puts &quot;Couldn't determine the browser&quot;
 }
 %&gt;
</pre>
</div>
<div id="subsection4" class="doctools_subsection"><h3><a name="subsection4">Example 2: Return full HTML page conditionally</a></h3>
<p>This example consists of a form, cookbook.html, that asks the user
whether they want to view a page with or without frames, and an ADP,
cookbook.adp, that determines the response and displays the
appropriate page, either the page with frames or the page without
frames.</p>
<p>This is the cookbook.html file containing the form:</p>
<pre class="doctools_example">
 &lt;html&gt;
 &lt;head&gt;&lt;title&gt;The ABC's of Fruit Cookbook&lt;/title&gt;
 &lt;/head&gt;
 &lt;body bggolor=&quot;#ffffff&quot;&gt;
 &lt;h1&gt;The ABC's of Fruit Cookbook&lt;/h1&gt;
 &lt;p&gt;
 How would you like to view this cookbook?
 &lt;form action=&quot;cookbook.adp&quot; method=&quot;POST&quot;&gt;
 &lt;input type=&quot;radio&quot; name=&quot;question&quot; value=&quot;yes&quot; CHECKED&gt;With Frames&lt;BR&gt;
 &lt;input type=&quot;radio&quot; name=&quot;question&quot; value=&quot;no&quot;&gt;Without Frames
 &lt;p&gt;
 &lt;input type=&quot;submit&quot; value=&quot;View Cookbook&quot;&gt;
 &lt;/form&gt;
 &lt;p&gt;
 &lt;/body&gt;
 &lt;/html&gt;
</pre>
<p>This is the ADP, cookbook.adp, that determines the response and
displays the appropriate page:</p>
<pre class="doctools_example">
 &lt;html&gt;
 &lt;head&gt;&lt;title&gt;The ABC's of Fruit Cookbook&lt;/title&gt;&lt;/head&gt;
 &lt;body bggolor=&quot;#ffffff&quot;&gt;
 &lt;%
  # Get form data and assign to variables
  set r [ns_conn form]
  set question [ns_set get $r question]
  # Display cookbook in appropriate format
  if {$question eq &quot;yes&quot;} {
      ns_adp_include cookset.html
  }  else {
      ns_adp_include cook.html
  }
 %&gt;
 &lt;/body&gt;
 &lt;/html&gt;
</pre>
<p>The cookset.html file contains a frame set for the cookbook. The
cook.html file contains the cookbook without frames.</p>
</div>
<div id="subsection5" class="doctools_subsection"><h3><a name="subsection5">Example 3: Return information from the database</a></h3>
<p>This example retrieves information from the database -- a list of
tables -- and returns it as the options in a select box. When the user
chooses a table from the list, another ADP is run as the POST for the
form which retrieves information from the database on the chosen
table.</p>
<p>The first ADP, db.adp, creates a form with a select box with the list
of database tables:</p>
<pre class="doctools_example">
 &lt;html&gt;
 &lt;head&gt;&lt;title&gt;DB Example&lt;/title&gt;&lt;/head&gt;
 &lt;body&gt;
 &lt;h1&gt;DB Example&lt;/h1&gt;
 
 &lt;p&gt;Select a db table from the default db pool:
 &lt;form method=&quot;POST&quot; action=&quot;db2.adp&quot;&gt;
 &lt;select name=&quot;Table&quot;&gt;
 &lt;%
  set db [ns_db gethandle]
  set sql &quot;select * from tables&quot;
  set row [ns_db select $db $sql]
 
  while {[ns_db getrow $db $row]} {
     set table [ns_set get $row name]
     ns_adp_puts &quot;&lt;option value=\&quot;$table\&quot;&gt;$table&quot;
  }
 %&gt;
 &lt;/select&gt;
 &lt;input type=&quot;submit&quot; value=&quot;Show Data&quot;&gt;
 &lt;/form&gt;
 &lt;/body&gt;
 &lt;/html&gt;
</pre>
<p>The second ADP, db2.adp, is used as the POST from the first ADP:</p>
<pre class="doctools_example">
 &lt;html&gt;
 &lt;head&gt;&lt;title&gt;DB Example page 2&lt;/title&gt;&lt;/head&gt;
 &lt;body&gt;
 &lt;h1&gt;DB Example page 2&lt;/h1&gt;
 
 &lt;%
  set table [ns_set get [ns_conn form] Table]
  set db [ns_db gethandle]
 %&gt;
 
 Contents of &lt;%= $table %&gt;:
 &lt;table border=&quot;1&quot;&gt;
 &lt;%
   set row [ns_db select $db &quot;select * from $table&quot;]
   set size [ns_set size $row]
   while {[ns_db getrow $db $row]} {
       ns_adp_puts &quot;&lt;tr&gt;&quot;
       for {set i 0} {$i &lt; $size} {incr i} {
 	     ns_adp_puts &quot;&lt;td&gt;[ns_set value $row $i]&lt;/td&gt;&quot;
       }
       ns_adp_puts &quot;&lt;/tr&gt;&quot;
   }
 %&gt;
 &lt;/table&gt;
 
 &lt;/body&gt;
 &lt;/html&gt;
</pre>
</div>
<div id="subsection6" class="doctools_subsection"><h3><a name="subsection6">Example 4: Get form information and insert into the database</a></h3>
<p>This is another database example, but one where the user types
information into a form, and the submit runs an ADP that enters the
information into the database. Then it sends an email message to both
the db administrator and the user that the record was updated. The
survey.html file contains the form and calls the survey.adp file as
the POST action.</p>
<p>Here is the survey.html file, which consists of a simple form and a
submit button which calls an ADP:</p>
<pre class="doctools_example">
 &lt;html&gt;
 &lt;head&gt;&lt;title&gt;Survey Form&lt;/title&gt;
 &lt;/head&gt;
 &lt;body bggolor=&quot;#ffffff&quot;&gt;
 &lt;h2&gt;Online Newsletter Subscription&lt;/h2&gt;
 &lt;p&gt;
 &lt;i&gt;Sign up to be notified when this web site changes, or to
 receive an ASCII version via email. Thanks!&lt;/i&gt;
 
 &lt;form action=&quot;survey.adp&quot; method=&quot;POST&quot;&gt;
 
 &lt;b&gt;Name&lt;/b&gt; &lt;input type=&quot;text&quot; name=&quot;name&quot; size=&quot;40&quot;&gt;
 &lt;p&gt;&lt;b&gt;Title&lt;/b&gt;&lt;input type=&quot;text&quot; name=&quot;title&quot; size=&quot;40&quot; maxlength=&quot;80&quot;&gt;
 
 &lt;p&gt;&lt;input type=&quot;checkbox&quot; name=&quot;notify&quot; value=&quot;1&quot;&gt;Notify me by email
 when this newsletter changes online
 
 &lt;p&gt;&lt;input type=&quot;checkbox&quot; name=&quot;sendemail&quot; value=&quot;1&quot;&gt;Send me an ASCII
 version of this newsletter by email
 
 &lt;p&gt;&lt;b&gt;Email Address&lt;/b&gt;&lt;input type=&quot;text&quot; name=&quot;emailaddr&quot; size=&quot;40&quot; maxlength=&quot;60&quot;&gt;
 &lt;p&gt;&lt;input type=&quot;submit&quot;&gt;
 &lt;/form&gt;
 
 &lt;/body&gt;
 &lt;/html&gt;
</pre>
<p>Here is the survey.adp file, which gets the form data from the survey,
inserts it into the database, sends email to the subscription
administrator and the user, and displays a confirmation message:</p>
<pre class="doctools_example">
 &lt;html&gt;
 &lt;head&gt;&lt;title&gt;Subscription Processed Successfully&lt;/title&gt;
 &lt;/head&gt;
 &lt;body bggolor=&quot;#ffffff&quot;&gt;
 &lt;h2&gt;Online Newsletter Subscription&lt;/h2&gt;
 &lt;p&gt;
 Thank You for subscribing to our newsletter!
 
 &lt;%
  # Get form data and assign to variables
  set r [ns_conn form]
  set name [ns_set get $r name]
  set title [ns_set get $r title]
  set notify [ns_set get $r notify]
  set sendemail [ns_set get $r sendemail]
  set emailaddr [ns_set get $r emailaddr]
 
  # Set subscription options explicitly to 0 if not checked
  if {$notify ne 1} {set notify 0}
  if {$sendemail ne 1} {set sendemail 0}
 
  # Update database with new subscription
  set db [ns_db gethandle]
  ns_db dml $db &quot;insert into test values ([ns_dbquotevalue $name],
 					    [ns_dbquotevalue $title], $notify, $sendemail,
 					    [ns_dbquotevalue $emailaddr])&quot;
 
  # Send email message to subscription administrator
  set body &quot;A new newsletter subscription was added for &quot;
  append body $name
  append body &quot;. The database has been updated.&quot;
  ns_sendmail &quot;subscript@thecompany.com&quot; &quot;dbadmin@thecompany.com&quot; &quot;New Subscription&quot; $body
 
  # Send email message to user
  set body &quot;Your online newsletter subscription has been successfully processed.&quot;
  ns_sendmail $emailaddr &quot;dbadmin@thecompany.com&quot; &quot;Your Online Subscription&quot; $body
 
  # Show type of subscription to user
  if {$notify eq 1} {
     ns_adp_puts &quot;You will be notified via email when the online newsletter changes.&quot;
  }
  if {$sendemail eq 1} {
     ns_adp_puts &quot;Future issues of the newsletter will be sent to you via email.&quot;
  }
 %&gt;
 &lt;/body&gt;
 &lt;/html&gt;
</pre>
</div>
<div id="subsection7" class="doctools_subsection"><h3><a name="subsection7">Example 5: ADP sampler with includes, recursion, and streaming</a></h3>
<p>The following HTML is an example of a page containing several Tcl
scripts using the various ADP syntaxes. It invokes some Tcl functions,
includes a file, executes another ADP, and uses streaming.</p>
<pre class="doctools_example">
 &lt;html&gt;
 &lt;head&gt;&lt;title&gt;This is a test of ADP&lt;/title&gt;
 &lt;/head&gt;
 &lt;body&gt;
 &lt;h1&gt;This is a test of ADP&lt;/h1&gt;
 
 &lt;%
  ## Proxies should cache this page for a maximum of 1 hour:
  ns_setexpires 3600
  set host [ns_set iget [ns_conn headers] host]
 
  ## How many times has this page been accessed
  ## since the server was started?
  set count [nsv_incr . count 1]
 %&gt;
 
 Number of accesses since server start: &lt;%= $count %&gt;&lt;br&gt;
 tcl_version: &lt;%= $tcl_version %&gt;&lt;br&gt;
 tcl_library: &lt;%= $tcl_library %&gt;&lt;br&gt;
 Host: &lt;%= $host %&gt;&lt;br&gt;
 
 &lt;!-- Include the contents of a file: --&gt;
 &lt;% ns_adp_include standard-header %&gt;
 
 &lt;script language=&quot;tcl&quot; runat=&quot;server&quot;&gt;
 ## You can do recursive ADP processing as well:
 ns_adp_include time.adp
 &lt;/script&gt;
 
 &lt;p&gt;Here's an example of streaming:
 &lt;script language=&quot;tcl&quot; stream=&quot;on&quot; runat=&quot;server&quot;&gt;
   ns_adp_puts &quot;&lt;br&gt;1...&lt;br&gt;&quot;
   ns_sleep 2
   ns_adp_puts &quot;2...&lt;br&gt;&quot;
   ns_sleep 2
   ns_adp_puts &quot;3!&lt;br&gt;&quot;
 &lt;/script&gt;
 &lt;p&gt;
 &lt;b&gt;End&lt;/b&gt;
 &lt;/body&gt;
 &lt;/html&gt;
</pre>
<p>The standard-header file referenced in the above example looks like this:</p>
<p>This is a standard header.</p>
<p>The time.adp file referenced in the example looks like this:</p>
<p>The time is: &lt;%=[ns_httptime [ns_time]]%&gt;</p>
<p>Because of the streaming used in the last script, the &quot;1...&quot;, &quot;2...&quot;,
&quot;3!&quot; and &quot;End&quot; part of the page will be displayed gradually.</p>
</div>
</div>
<div id="see-also" class="doctools_section"><h2><a name="see-also">See Also</a></h2>
<p>ns_adp, ns_adp_argv, ns_adp_eval, ns_adp_include, ns_adp_puts, ns_adp_register, ns_sleep</p>
</div>
<div id="keywords" class="doctools_section"><h2><a name="keywords">Keywords</a></h2>
<p><a href="../../index.html#adp">ADP</a>, <a href="../../index.html#configuration">configuration</a></p>
</div>
<div id="man-footer">
  
</div>
</div></body></html>
