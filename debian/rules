#!/usr/bin/make -f
# -*- makefile -*-
# Sample debian/rules that uses debhelper.
# This file was originally written by Joey Hess and Craig Small.
# As a special exception, when this file is copied by dh-make into a
# dh-make output file, you may use that output file without restriction.
# This special exception was added by Craig Small in version 0.37 of dh-make.

# Uncomment this to turn on verbose mode.
export DH_VERBOSE=1

# Fortify
export DEB_BUILD_MAINT_OPTIONS = hardening=+all
#export DEB_BUILD_MAINT_OPTIONS = hardening=+all,-pie

DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/buildflags.mk

# NaviServer version and ABI
export VERSION=$(shell grep NS_PATCH_LEVEL $(CURDIR)/include/nsversion.h | cut -d\" -f2)
export ABI=1

DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)
PREFIX=/usr/lib/$(DEB_HOST_MULTIARCH)/naviserver
PACKAGE=naviserver
COREPACKAGE=naviserver-core
TARGETDIR=$(CURDIR)/debian/tmp

override_dh_auto_test:

override_dh_auto_configure:
	# Generating debian/control
	sed -e "s/@VERSION@/$(VERSION)/g" -e "s/@ABI@/$(ABI)/g" < $(CURDIR)/debian/control.in > $(CURDIR)/debian/control
	# Generating debian/debian.substvars
	sed -e "s/@VERSION@/$(VERSION)/g" -e "s/@ABI@/$(ABI)/g" < $(CURDIR)/debian/debian.substvars.in > $(CURDIR)/debian/debian.substvars
	# Setting ABI in modules makefile for soname
	sed -i "s/@ABI@/$(ABI)/g" $(CURDIR)/include/Makefile.module.in
	# Configuring
	dh_auto_configure -- --with-tcl=/usr/lib/$(DEB_HOST_MULTIARCH)/tcl8.6 --prefix=$(PREFIX) --enable-symbols --enable-threads --mandir=/usr/share/man --disable-rpath

override_dh_auto_install:
	$(MAKE) NAVISERVER=$(TARGETDIR) install
	# Rename nsd and nsproxy
	mv $(TARGETDIR)/bin/nsd $(TARGETDIR)/bin/$(PACKAGE)-nsd
	mv $(TARGETDIR)/bin/nsproxy $(TARGETDIR)/bin/$(PACKAGE)-nsproxy
	# Default config file
	cp $(CURDIR)/sample-config.tcl $(TARGETDIR)/default.tcl
	cp $(CURDIR)/debian/default.sh $(TARGETDIR)/.
	# substvars
	cp $(CURDIR)/debian/debian.substvars $(TARGETDIR)/.
	# Move headers and Makefiles to -dev package
	cp $(CURDIR)/nsdb/*.h $(TARGETDIR)/include/
	cp $(CURDIR)/include/Makefile.global $(TARGETDIR)/include/
	cp $(CURDIR)/include/Makefile.module $(TARGETDIR)/include/
	# Strip srcdir from Makefile.global for reproducibility
	sed -i -e '/^srcdir[[:space:]]*=.*/d' $(TARGETDIR)/include/Makefile.global
	# Default index page
	rm -f $(TARGETDIR)/pages/index.adp
	cp $(CURDIR)/index.adp $(TARGETDIR)/
	cp $(TARGETDIR)/pages/doc/naviserver/man.css $(TARGETDIR)/index.css
	# Delete empty directory
	rmdir $(TARGETDIR)/modules/tcl
	# Delete install-sh
	rm -f $(TARGETDIR)/bin/install-sh
	# Install the systemd generator
	install -d $(TARGETDIR)/lib/systemd/system-generators/
	install $(CURDIR)/debian/naviserver-generator $(TARGETDIR)/lib/systemd/system-generators/
	# Copy the README file about the cgi-bin directory
	cp $(CURDIR)/debian/README.cgi-bin $(TARGETDIR)/.
	# Copy the naviserver NEWS file
	cp $(CURDIR)/NEWS $(TARGETDIR)/.

# Disable dh_missing due to false positives
override_dh_missing:

override_dh_install:
	dh_install --sourcedir=$(TARGETDIR)

override_dh_auto_clean:
	rm -f $(CURDIR)/tests/testserver/*.log
	rm -f $(CURDIR)/tests/testserver/*.pid
	# Disabled as it deletes include/Makefile.global before build
	#dh_auto_clean

override_dh_compress:
	# Example config files are not really useful in compressed state
	dh_compress -Xconfig.tcl

override_dh_shlibdeps:
	dh_shlibdeps -a -L$(COREPACKAGE)

override_dh_clean:
	rm -f $(CURDIR)/debian/debian.substvars
	dh_clean
%:
	dh $@
