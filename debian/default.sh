#
# These variables can be customized to change main NaviServer settings.
# More changes can be done by modifying the $CONF Tcl script.
# 
# Note that these variables are read and interpreted by the Tcl script
# too, so avoid using shell capabilities in setting vars.
#

USER=www-data
GROUP=www-data
HOSTNAME=localhost
ADDRESS=127.0.0.1
HTTP_PORT=80
HTTPS_PORT=443
CONF=/etc/naviserver/conf.d/default.tcl
RUN_DAEMON=yes
